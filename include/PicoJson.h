#ifdef __cplusplus

#include <PicoJson.hh>

#else

#error PicoJson requires a C++11 compiler. Try changing file extension to .cc or .cpp

#endif