#ifndef PICO_JSON_H
#define PICO_JSON_H

// PicoJson version
#define PJ_VERSION_MAJOR 1
#define PJ_VERSION_MINOR 0
#define PJ_VERSION_PATCH 0


#include <parse/StaticParser.hh>
#include <parse/SlabParser.hh>
#include <parse/DynParser.hh>

#include <parse/ParseError.hh>
#include <parse/JsonValue.hh>
#include <parse/JsonStructure.hh>

#include <util/Polyfills.hh>
#include <util/Namespace.hh>


namespace pj {
  using PJ_IMPL_NAMESPACE::StaticParser;
  using PJ_IMPL_NAMESPACE::SlabParser;
  using PJ_IMPL_NAMESPACE::DynParser;

  using PJ_IMPL_NAMESPACE::ParseError;
  using PJ_IMPL_NAMESPACE::JsonValue;
  using PJ_IMPL_NAMESPACE::JsonValueType;
  using PJ_IMPL_NAMESPACE::JsonStructure;

  using PJ_IMPL_NAMESPACE::move;
  using PJ_IMPL_NAMESPACE::fnat_t;
}

#endif
