#ifndef BYTES_BITS_H
#define BYTES_BITS_H

#include <stdint.h>

#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

/**
 * How many bytes to fit nbits? (Always returns at least 1)
 */
constexpr uint16_t bytesForBitsetQuick(const uint16_t nbits) noexcept {
  return (nbits >> 3) + 1;
}

/**
 * How many bytes to fit nbits?
 */
constexpr uint16_t bytesForBitset(const uint16_t nbits) noexcept {
  return nbits == 0 ? 0 : bytesForBitsetQuick(nbits);
}

}

#endif
