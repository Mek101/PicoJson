#ifndef NUM_PARSE_H
#define NUM_PARSE_H

#include <stdint.h>

#include <util/WellKnown.hh>
#include <util/Namespace.hh>

#if PJ_INTEGER_ONLY_NUM_PARSE == 1
#include <util/convert/num_parse_types/NumIntParseTypes.hh>
#else
#include <util/convert/num_parse_types/NumParseTypes.hh>
#endif


namespace PJ_IMPL_NAMESPACE {

/**
 * The buffer is expected to contain only the number and nothing else.
 * 
 * @param str a c-style string with the number to parse.
 */
NumParseResult tryParseNumber(const char* str) noexcept;

}

#endif
