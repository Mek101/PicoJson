#ifndef UTF16_PARSE_H
#define UTF16_PARSE_H

#if PJ_ESCAPED_UNICODE == 1

#include <stdint.h>

#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

enum class UTF16ConversionError : uint8_t {
  // The buffer's content was successfully replaced with UTF-8.
  None,
  // The value of the hax character units is not valid unicode. Replaced with unicode's replacement character.
  InvalidCodepoint,
  // The buffer contains only a high surrogate, requiring a low surrogate to make a pair.
  IncompletePair,
};

struct UTF16ConversionResult {
  UTF16ConversionError err;
  int8_t written;
};

/**
 * Attempts to convert in-place a string with hex characters representing a UTF-16 codepoint or surrogate pair.
 * SAFETY: buff is assumed to contain only valid hex characters.
 * 
 * @param buff a character buffer which's size if a multiple of 4, each 4 characters being valid UTF-16 hex characters.
 * @param isPairs if the buffer contains two UTF-16 codepoints.
 * 
 * @return the conversion error, if any.
 */
UTF16ConversionResult tryConvertInplace(char* buff, const bool isPairs) noexcept;

}

#endif

#endif
