#ifndef NUM_PARSE_INT_TYPES_H
#define NUM_PARSE_INT_TYPES_H

#if PJ_INTEGER_ONLY_NUM_PARSE == 1

#include <util/Polyfills.hh>
#include <util/WellKnown.hh>
#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

enum class NumParseError : uint8_t {
  IntegerNone = WellKnown::IntegerErrNone,
  IntegerOverflow = WellKnown::IntegerOverflow,
  Invalid,
};

struct NumParseResult {
  NumParseError err;
  int32_t iValue;
};


constexpr bool isNumberCharacter(const char c) {
  return isDigit(c) || c == '-';
}

}

#endif

#endif
