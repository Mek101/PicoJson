#ifndef NUM_PARSE_TYPES_H
#define NUM_PARSE_TYPES_H

#if PJ_INTEGER_ONLY_NUM_PARSE == 0

#include <util/Polyfills.hh>
#include <util/WellKnown.hh>
#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

enum class NumParseError : uint8_t {
  IntegerNone = WellKnown::IntegerErrNone,
  IntegerOverflow = WellKnown::IntegerOverflow,
  FloatNone = WellKnown::FloatErrNone,
  FloatOverflow = WellKnown::FloatOverflow,
  FloatUnderflow = WellKnown::FloatUnderflow,
  Invalid,
};

struct NumParseResult {
  NumParseError err;
  union {
    fnat_t fValue;
    int32_t iValue;
  };
};


constexpr bool isNumberCharacter(const char c) {
  return isDigit(c) || c == '.' || c == '+' || c == '-' || (c & 0x5f) == 'E';
}

}

#endif

#endif
