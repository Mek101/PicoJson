#ifndef FLOAT_NAT_H
#define FLOAT_NAT_H


namespace PJ_IMPL_NAMESPACE {

#if defined(ARDUINO)
using fnat_t = double;
#else
using fnat_t = float;
#endif

}

#endif
