#ifndef NULLPTR_H
#define NULLPTR_H

#include <util/Namespace.hh>


#if defined(ARDUINO)

#include <stddef.h>

namespace PJ_IMPL_NAMESPACE {
  using nullptr_t = nullptr_t;
}

#else

#include <cstddef>

namespace PJ_IMPL_NAMESPACE {
  using nullptr_t = std::nullptr_t;
}

#endif

#endif
