#ifndef MOVE_H
#define MOVE_H

#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

template<class T> struct remove_reference {
  using type = T;
};

template<class T> struct remove_reference<T&> {
  using type = T;
};

template<class T> struct remove_reference<T&&> {
  using type = T;
};

template<class T> using remove_reference_t = typename remove_reference<T>::type;


/**
 * Used by pj_impl::forward.
 */
template<typename> struct is_lvalue_reference {
  static constexpr bool value = false;
};

/**
 * Used by pj_impl::forward.
 */
template<typename T> struct is_lvalue_reference<T&> {
  static constexpr bool value = true;
};


/**
 * Equivalent to std::move().
 * PicoJson parsers aren't copyable, and some embedded STL ports don't include std::move(). This is exposed as a public
 * API for the implementor's and consumer's convenience.
 */
template<typename T> constexpr remove_reference_t<T>&& move(T&& obj) {
  return static_cast<remove_reference_t<T>&&>(obj);
}

/**
 * Equivalent to std::forward. Implements "perfect forwarding".
 */
template<typename T> constexpr T&& forward(typename remove_reference<T>::type& args) noexcept {
  return static_cast<T&&>(args);
}

/**
 * Equivalent to std::forward. Implements "perfect forwarding".
 */
template<typename T> constexpr T&& forward(typename remove_reference<T>::type&& args) noexcept {
  static_assert(!is_lvalue_reference<T>::value, "Cannot convert an rvalue to an lvalue");
  return static_cast<T&&>(args);
}

}

#endif
