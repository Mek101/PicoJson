#ifndef TYPE_H
#define TYPE_H

#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

constexpr bool isDigit(const char c) {
  return c >= '0' && c <= '9';
}

constexpr bool isHexDigit(const char c) {
  return (c >= '0' && c <= '9') || ((c & 0x5F) >= 'A' && (c & 0x5F) <= 'F');
}

}

#endif
