#ifndef INLINE_MACROS_H
#define INLINE_MACROS_H

#if defined(__GNUC__) || defined(__clang__)
#define PJ_NO_INLINE __attribute__((noinline))
#define PJ_FORCE_INLINE __attribute__((always_inline)) inline
#elif defined(_MSC_VER)
#define PJ_NO_INLINE __declspec(noinline)
#define PJ_FORCE_INLINE __forceinline
#elif
#define PJ_NO_INLINE
#define PJ_FORCE_INLINE inline
#endif

#endif
