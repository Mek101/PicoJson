#if !defined(PJ_VERSION_MAJOR) || !defined(PJ_VERSION_MINOR) || !defined(PJ_VERSION_PATCH)
#error "Project version not fully defined"
#else

#define VERSIONED_NAMESPACE_IMPL(a, b, c) pj_impl_ ## a ## _ ## b ## _ ## c
#define VERSIONED_NAMESPACE(a, b, c) VERSIONED_NAMESPACE_IMPL(a, b, c)
#define PJ_IMPL_NAMESPACE VERSIONED_NAMESPACE(PJ_VERSION_MAJOR, PJ_VERSION_MINOR, PJ_VERSION_PATCH)

#endif
