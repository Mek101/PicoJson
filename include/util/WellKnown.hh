#ifndef WELL_KNOWN_H
#define WELL_KNOWN_H

#include <stdint.h>

#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

#if PJ_INTEGER_ONLY_NUM_PARSE == 1
enum WellKnown {
  StringErrNone = 0x0,
  StringTruncated = 0x1,
  IntegerErrNone = 0x2,
  IntegerOverflow = 0x3,
};
#else
enum WellKnown {
  StringErrNone = 0x0,
  StringTruncated = 0x1,
  IntegerErrNone = 0x2,
  IntegerOverflow = 0x3,
  FloatErrNone = 0x4,
  FloatOverflow = 0x5,
  FloatUnderflow = 0x6,
};
#endif

template<typename F, typename T> constexpr T wellKnownTransmute(const F from) {
  return static_cast<T>(static_cast<uint8_t>(from));
}

};

#endif
