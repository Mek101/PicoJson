#ifndef DEFERRED_ARENA_IMPL_H
#define DEFERRED_ARENA_IMPL_H

#include <stdint.h>


struct DeferredArenaImpl {
  void* buffer;
  uint16_t size;

  constexpr DeferredArenaImpl() : buffer(nullptr), size(0) {}
  constexpr explicit DeferredArenaImpl(void *const buffer, const uint16_t size) noexcept : buffer(buffer), size(size) {}

  constexpr DeferredArenaImpl(const DeferredArenaImpl& other) noexcept : buffer(other.buffer), size(other.size) {}
  DeferredArenaImpl& operator=(const DeferredArenaImpl& other) noexcept = default;
};

#endif
