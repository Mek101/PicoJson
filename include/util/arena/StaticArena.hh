#ifndef STATIC_ARENA_H
#define STATIC_ARENA_H

#include <stdint.h>

#include <util/Namespace.hh>
#include <util/Polyfills.hh>


namespace PJ_IMPL_NAMESPACE {

template<typename T, uint16_t SIZE> class StaticArena {
private:
  T buffer[SIZE];

public:
  StaticArena() = default;

  StaticArena(const StaticArena& other) = delete;
  StaticArena& operator=(const StaticArena& other) = delete;

  StaticArena(StaticArena&& other) noexcept = default;
  StaticArena& operator=(StaticArena&& other) noexcept = default;


  T& operator[](const uint16_t index) noexcept {
    return this->buffer[index];
  }

  constexpr uint16_t available() const noexcept {
    return SIZE;
  }

  constexpr bool setAvailable(const uint16_t size) const noexcept {
    return size <= SIZE;
  }

  T* rawBuffer() noexcept {
    return this->buffer;
  }

  constexpr uint16_t capacity() const noexcept {
    return SIZE;
  }
};


template<typename T> class StaticArena<T, 0> {
  /**
   * Implementation notes: 0-sized arrays are illegal in standard C++.
   * This specialized template exists to handle the case.
   */
public:
  StaticArena() = default;

  StaticArena(const StaticArena& other) = delete;
  StaticArena& operator=(const StaticArena& other) = delete;

  StaticArena(StaticArena&& other) noexcept = default;
  StaticArena& operator=(StaticArena&& other) noexcept = default;


  /**
   * SAFETY: This is always UNDEFINED BEHAVIOR, but must be implemented to conform to the interface.
   */
  constexpr T& operator[](const uint16_t index) const noexcept {
    return nullptr_t();
  }

  constexpr uint16_t available() const noexcept {
    return 0;
  }

  constexpr bool setAvailable(const uint16_t size) const noexcept {
    return size == 0;
  }

  constexpr T* rawBuffer() const noexcept {
    return nullptr;
  }

  constexpr uint16_t capacity() const noexcept {
    return 0;
  }
};

}

#endif
