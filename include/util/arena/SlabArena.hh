#ifndef SLAB_ARENA_H
#define SLAB_ARENA_H

#include <stdint.h>

#include <util/arena/DeferredArenaImpl.hh>

#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

template<typename T> class SlabArena {
private:
  DeferredArenaImpl impl;
public:
  SlabArena() = default;
  explicit SlabArena(T* const buffer, const uint16_t size) : impl(buffer, size) {}

  SlabArena(const SlabArena& other) = delete;
  SlabArena& operator=(const SlabArena& other) = delete;

  SlabArena(SlabArena&& other) noexcept = default;
  SlabArena& operator=(SlabArena&& other) noexcept = default;


  constexpr T& operator[](const uint16_t index) noexcept {
    return static_cast<T*>(this->impl.buffer)[index];
  }

  constexpr uint16_t available() const noexcept {
    return this->impl.size;
  }

  constexpr bool setAvailable(const uint16_t size) const noexcept {
    return size <= this->impl.size;
  }

  constexpr T* rawBuffer() noexcept {
    return static_cast<T*>(this->impl.buffer);
  }

  constexpr uint16_t capacity() const noexcept {
    return this->impl.size;
  }
};

}

#endif
