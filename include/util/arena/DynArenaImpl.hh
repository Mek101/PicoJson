#ifndef DYN_ARENA_IMPL_H
#define DYN_ARENA_IMPL_H

#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>

#include <util/arena/DeferredArenaImpl.hh>

#include <util/Polyfills.hh>
#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

class DynArenaImpl {
public:
  DeferredArenaImpl impl;

  uint16_t maxCount;
  uint8_t minCount;
private:
  /**
   * Clamps the given size to a minimum size and prevent an overflow.
   */
  static constexpr uint16_t clampMaxSize(uint16_t size, const size_t objSize) noexcept {
    // Check if the given size allows for an overflow.
    // If the current maximum size allows for an overflow to occur, return the maximum allocable without an overflow.
    return size > (UINT16_MAX / objSize) ? (UINT16_MAX / objSize) : size;
  }

  bool resizeAlloc(const uint16_t size, const size_t objSize) noexcept;

public:
  constexpr explicit DynArenaImpl(const uint16_t maxSize, const uint8_t minSize, const size_t objSize) noexcept : impl(nullptr, 0), maxCount(clampMaxSize(maxSize, objSize)), minCount(minSize) {}

  inline ~DynArenaImpl() noexcept {
    free(this->impl.buffer);
  }

  // Non-copyable
  DynArenaImpl(const DynArenaImpl& other) = delete;
  DynArenaImpl& operator=(const DynArenaImpl& other) = delete;

  DynArenaImpl(DynArenaImpl&& other) noexcept;

  inline DynArenaImpl& operator=(DynArenaImpl&& other) noexcept {
    *this = DynArenaImpl(move(other));
    return *this;
  }

  inline uint16_t available() const noexcept {
    return this->impl.size;
  }

  bool setAvailable(const uint16_t size, const size_t objSize) noexcept;

  inline void* rawBuffer() noexcept {
    return this->impl.buffer;
  }
};

}

#endif
