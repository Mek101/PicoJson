#ifndef DYNAMIC_ARENA_H
#define DYNAMIC_ARENA_H

#include <stdint.h>

#include <util/Namespace.hh>
#include <util/Polyfills.hh>

#include <util/arena/DynArenaImpl.hh>


namespace PJ_IMPL_NAMESPACE {

/**
 * CORRECTNESS: T must be a trivially copyable type.
 * T: the type to be stored in the arena
 * MIN_ALLOC: the base allocation for any setAvailable request lesser than itself.
 */
template<typename T, uint8_t MIN_ALLOC> class DynArena {
  static_assert(MIN_ALLOC >= 2, "MIN_ALLOC must be 2 or greater");
private:
  DynArenaImpl impl;
public:
  DynArena() noexcept : impl(0, MIN_ALLOC, sizeof(T)) {}
  explicit DynArena(const uint16_t maxSize) noexcept : impl(maxSize, MIN_ALLOC, sizeof(T)) {}

  // Non-copyable
  DynArena(const DynArena& other) = delete;
  DynArena& operator=(const DynArena& other) = delete;

  DynArena(DynArena&& other) noexcept = default;

  DynArena& operator=(DynArena&& other) noexcept = default;


  inline T& operator[](const uint16_t index) noexcept {
    return static_cast<T*>(this->impl.rawBuffer())[index];
  }

  constexpr uint16_t available() const noexcept {
    return this->impl.available();
  }

  bool setAvailable(const uint16_t size) noexcept {
    return this->impl.setAvailable(size, sizeof(T));
  }

  inline T* rawBuffer() noexcept {
    return static_cast<T*>(this->impl.rawBuffer());
  }

  constexpr uint16_t capacity() const noexcept {
    return this->impl.maxCount;
  }
};

}

#endif
