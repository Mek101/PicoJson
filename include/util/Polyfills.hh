#ifndef POLYFILLS_H
#define POLYFILLS_H

#include <util/Namespace.hh>

#include <util/polyfills/InlineMacros.hh>
#include <util/polyfills/Utility.hh>
#include <util/polyfills/Type.hh>
#include <util/polyfills/Nullptr.hh>
#include <util/polyfills/FloatNat.hh>

#endif
