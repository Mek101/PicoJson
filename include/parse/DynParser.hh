#ifndef DYN_PARSER_H
#define DYN_PARSER_H

#include <stdint.h>

#include <parse/ParseError.hh>

#include <parse/impl/BaseParser.hh>
#include <parse/impl/CharBuffer.hh>
#include <parse/impl/PathStack.hh>

#include <util/arena/DynArena.hh>
#include <util/BytesBits.hh>
#include <util/Polyfills.hh>
#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

/**
 * A json parser with two heap allocated resizable buffers.
 * The two heap allocations start small and are resized like a vector depending on the buffers actual usage.
 * Suggested if the consumer expects a rare string much longer than the average, or a section of the document with much
 * deeper nesting.
 */
template<typename H> class DynParser {
private:
  template<typename T> using PCArena = DynArena<T, 8>;
  template<typename T> using PSArena = DynArena<T, 2>;
  using DBaseParser = BaseParser<PCArena, PSArena, H>;

  DBaseParser impl;
public:
  constexpr explicit DynParser(const uint16_t charBufferSize, const uint16_t maxDocumentDepth, const H& handle) noexcept
    : impl(move(PCArena<char>(charBufferSize)), move(PSArena<uint8_t>(bytesForBitset(maxDocumentDepth))), maxDocumentDepth, handle) {}

  // Non-copyable
  DynParser(const DynParser& other) = delete;
  DynParser& operator=(const DynParser& other) = delete;

  // Movable.
  DynParser(DynParser&& other) noexcept = default;
  DynParser& operator=(DynParser&& other) noexcept = default;


  /**
   * Advances the parser state with the given character. The terminator character is always ignored.
   * @return an error reporting the parser's state.
   */
  ParseError parse(const char c) {
    return this->impl.parse(c);
  }

  /**
   * Reset the parser to it's initial default state.
   */
  void reset() noexcept {
    this->impl.reset();
  }

  H& handle() noexcept {
    return this->impl.handle;
  }
};

}

#endif
