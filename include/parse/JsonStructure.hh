#ifndef JSON_STRUCTURE_H
#define JSON_STRUCTURE_H

#include <stdint.h>

#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

/**
 * The structure types provided for by the json specification(s).
 */
enum class JsonStructure : uint8_t {
  // A json array.
  Array,
  // A json object.
  Object
};

}

#endif
