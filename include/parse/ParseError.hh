#ifndef PARSE_ERROR_H
#define PARSE_ERROR_H

#include <stdint.h>

#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

/**
 * The errors (or lack thereof) that the parsers may incur into when advanced.
 */
enum class ParseError : uint8_t {
  // Success.
  None,

  /**
   * Non-fatal errors.
   */
  // Overflow of the main character buffer while accumulating a string. Truncating the string.
  StringOverflow,
  // Unescaped control character encountered. Skipping the character.
  UnescapedControlCharacter,
  // Invalid unicode codepoint was replaced with U+0xFFFD.
  BadUnicodeCodepoint,

  /**
   * Malformed input fatal errors.
   */
  // The document did not start with a json structure.
  BadDocument,
  // Malformed array.
  BadArray,
  // Malformed object.
  BadObject,
  // Malformed object key.
  BadKey,
  // Malformed value.
  BadValue,
  // Malformed number.
  BadNumber,
  // Malformed boolean or null.
  BadLiteral,
  // Bad escape character in string.
  BadEscapeCharacter,
  // Malformed escaped unicode.
  BadUnicode,

  /**
   * Fatal parser errors and resource exhaustion.
   */
  // The parser has either successfully completed the document or was interrupted by a fatal error, and must be reset.
  Complete,
  // The parser has reached an invalid state. This is a bug.
  InvalidState,
  // Overflow of the character buffer.
  BufferOverflow,
  // Overflow of the maximum document depth.
  DepthOverflow,
};

}

#endif
