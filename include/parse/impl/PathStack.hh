#ifndef STACK_PATH_H
#define STACK_PATH_H

#include <stdint.h>
#include <string.h>

#include <parse/impl/JsonStructureType.hh>

#include <util/BytesBits.hh>
#include <util/Polyfills.hh>


namespace PJ_IMPL_NAMESPACE {

/**
 * A stack holding the nested json structures
 */
template<template<typename> class arena_t> class PathStack {
  /**
   * @note The stack path is implemented as a stack of bits over an arena.
   */
private:
  uint16_t maxBits;
  uint16_t next;
  arena_t<uint8_t> arena;

  /**
   * SAFETY: index is within maxBits.
   */
  bool test(const uint16_t index) {
    const uint16_t byteIndex = index / 8;
    const uint8_t offset = index % 8;

    return (this->arena[byteIndex] >> offset) & 0x1;
  }

  /**
   * SAFETY: index is within maxBits.
   */
  void set(const uint16_t index, const bool value) {
    const uint16_t byteIndex = index / 8;
    const uint8_t offset = index % 8;

    uint8_t bitfield = uint8_t(1 << offset);

    if (value) {
      // Bit on.
      this->arena[byteIndex] |= bitfield;
    } else {
      // Turn off bit.
      this->arena[byteIndex] &= (~bitfield);
    }
  }
public:
  PathStack() = default;

  /**
   * SAFETY: the caller must guarantee that numBits bits will fit into the given arena.
   */
  explicit PathStack(arena_t<uint8_t>&& arena, const uint16_t numBits)
    : maxBits(numBits), next(0), arena(move(arena)) {}


  void reset() {
    this->next = 0;
    this->arena.setAvailable(0);
  }

  /**
   * SAFETY: type must not be invalid.
   */
  bool push(const JsonStructureType type) {
    if (this->next == this->maxBits) {
      return false;
    }

    // Ensure to have available space.
    auto requiredBytes = bytesForBitsetQuick(this->next);
    if (!this->arena.setAvailable(requiredBytes)) {
      return false;
    }

    this->set(this->next, type == JsonStructureType::Array);
    this->next++;
    return true;
  }

  JsonStructureType pop() {
    if (this->next == 0) {
      return JsonStructureType::Invalid;
    }

    this->next--;
    auto res = this->test(this->next) ? JsonStructureType::Array : JsonStructureType::Object;

    // Reduce the required space.
    auto requiredBytes = bytesForBitsetQuick(this->next);
    this->arena.setAvailable(requiredBytes);
    return res;
  }

  void popDestroyUnchecked() {
    this->next--;
    auto requiredBytes = bytesForBitsetQuick(this->next);
    this->arena.setAvailable(requiredBytes);
  }

  JsonStructureType peekHead() {
    if (this->next == 0) {
      return JsonStructureType::Invalid;
    }
    return this->test(this->next - 1) ? JsonStructureType::Array : JsonStructureType::Object;
  }

  uint16_t size() const {
    return this->next;
  }
};

}

#endif
