#ifndef CHAR_BUFFER_H
#define CHAR_BUFFER_H

#include <stdint.h>

#include <util/Polyfills.hh>
#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

template<template<typename> class arena_t> class CharBuffer {
private:
  uint16_t next;
  arena_t<char> arena;
public:
  CharBuffer() = default;
  explicit CharBuffer(arena_t<char>&& arena) : next(0), arena(move(arena)) {}


  /**
   * Clear the buffer while potentially modifying the internal arena.
   */
  void reset() {
    this->next = 0;
    this->arena.setAvailable(0);
  }

  /**
   * Clear the buffer without modifying the internal arena.
   */
  void clear() {
    this->next = 0;
  }

  const char* rawBuffer() {
    return this->arena.rawBuffer();
  }

  char* sliceFromEndUnchecked(const uint16_t size) {
    return this->arena.rawBuffer() + (this->next - size);
  }

  uint16_t bufferLength() const {
    return this->next;
  }

  bool pushChar(const char c) {
    if (this->next == this->arena.capacity() || !this->arena.setAvailable(this->next)) {
      return false;
    }

    this->arena[this->next] = c;
    this->next++;
    return true;
  }

  void pushCharUnchecked(const char c) {
    this->arena[this->next] = c;
    this->next++;
  }

  bool pushOrReplaceChar(const char c) {
    if (this->arena.capacity() == 0) {
      return false;
    } else if (this->next == this->arena.capacity() || !this->arena.setAvailable(this->next)) {
      this->arena[this->next - 1] = c;
      return false;
    } else {
      this->arena[this->next] = c;
      this->next++;
      return true;
    }
  }

  bool ensureAvailable(const uint16_t count) {
    return this->arena.setAvailable(this->next + count);
  }

  bool popDestroyBy(const uint16_t count) {
    if (this->next < count) {
      return false;
    }

    this->next -= count;
    this->arena.setAvailable(this->next);

    return true;
  }
};

}

#endif
