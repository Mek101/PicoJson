#ifndef JSON_VALUE_W_H
#define JSON_VALUE_W_H

#include <parse/JsonValue.hh>

#include <parse/impl/JsonVariant.hh>

#include <util/Polyfills.hh>
#include <util/WellKnown.hh>


namespace PJ_IMPL_NAMESPACE {

class JsonValueW : public JsonValue {
public:
  PJ_FORCE_INLINE JsonValueW() noexcept {}
  PJ_FORCE_INLINE JsonValueW(const JsonValueTypeImpl type, const JsonVariant variant) noexcept : JsonValue(type, variant) {}
};

}

#endif
