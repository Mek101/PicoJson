#ifndef BASE_MINI_JSON_PARSER_H
#define BASE_MINI_JSON_PARSER_H

#include <stdint.h>

#include <parse/ParseError.hh>
#include <parse/JsonStructure.hh>

#include <parse/impl/JsonStructureType.hh>
#include <parse/impl/JsonValueW.hh>
#include <parse/impl/CharBuffer.hh>
#include <parse/impl/PathStack.hh>

#include <util/Polyfills.hh>
#include <util/Namespace.hh>
#include <util/Convert.hh>


namespace PJ_IMPL_NAMESPACE {

template<template<typename> class CharArena_t, template<typename> class StackArena_t, typename Handle_t> class BaseParser {
public:
  // Some wrappers rely on having access on the types they supply.

  // Character buffer.
  CharBuffer<CharArena_t> charBuffer;
  // Stack to keep track of the traversed json structures.
  PathStack<StackArena_t> pathStack;

  // User supplied handle.
  Handle_t handle;
private:
  enum class ParseState : uint8_t {
    // Document ended. The parser must be reset.
    DocumentEnd,
    // State potentially corrupted. Abort the input and reset.
    Errored,
    DocumentBegin,

    InArray,
    InObject,
    AfterKey,
    EndKey,

    // SAFETY: from here forward, isParsingTokenState relies on the enum values being adjacent.
    InKeyString,
    // Escaped key string.
    InEscapeKeyBegin,

    InValueString,
    // Escaped value string.
    InEscapeValueBegin,

    // The 0 state is elided.
    InValueTrue1,
    InValueTrue2,
    InValueTrue3,

    // The 0 state is elided.
    InValueFalse1,
    InValueFalse2,
    InValueFalse3,
    InValueFalse4,

    // The 0 state is elided.
    InValueNull1,
    InValueNull2,
    InValueNull3,

    InValueNumber,

#if PJ_ESCAPED_UNICODE == 1
    /**
     * Unicode states: set aside to better encode the state in the single bits.
     * 1000 0000
     * - The most significant bit (MSB) is reserved for unicode states.
     * - The second MSB indicates if it's in a key or a value.
     *   - 0: a key
     *   - 1: a value
     * - The third MSB indicates if it's the first unicode codepoint/high surrogate or the low surrogate of a pair.
     *   - 0: a codepoint/high surrogate.
     *   - 1: a low surrogate.
     */

    // Exactly 4 unicode characters for the codeunit or high surrogate (hence the CH suffix).
    // SAFETY: the code relies on these states to be adjacent.
    InKeyUnicodeCH0 = 0x80,
    InKeyUnicodeCH1 = 0x81,
    InKeyUnicodeCH2 = 0x82,
    InKeyUnicodeCH3 = 0x83,

    InKeyAfterUnicodeHighSurrogate0 = 0x90,
    InKeyAfterUnicodeHighSurrogate1 = 0x91,

    // Exactly 4 unicode characters for the low surrogate (hence the L suffix).
    // SAFETY: the code relies on these states to be adjacent.
    InKeyUnicodeL0 = 0xA0,
    InKeyUnicodeL1 = 0xA1,
    InKeyUnicodeL2 = 0xA2,
    InKeyUnicodeL3 = 0xA3,


    // Exactly 4 unicode characters for the codeunit or high surrogate (hence the CH suffix).
    // SAFETY: the code relies on these states to be adjacent.
    InValueUnicodeCH0 = 0xC0,
    InValueUnicodeCH1 = 0xC1,
    InValueUnicodeCH2 = 0xC2,
    InValueUnicodeCH3 = 0xC3,

    InValueAfterUnicodeHighSurrogate0 = 0xD0,
    InValueAfterUnicodeHighSurrogate1 = 0xD1,

    // Exactly 4 unicode characters for the low surrogate (hence the L suffix).
    // SAFETY: the code relies on these states to be adjacent.
    InValueUnicodeL0 = 0xE0,
    InValueUnicodeL1 = 0xE1,
    InValueUnicodeL2 = 0xE2,
    InValueUnicodeL3 = 0xE3
#endif
      AfterValue
  };


  ParseState state;


  //***************************************************************************
  // Pure util functions.
  //***************************************************************************
  /**
   * @return true if the state indicates the parser is inside a token where whitespace matters (cannot be ignored).
   */
  static constexpr bool isParsingTokenState(const ParseState state) noexcept {
    return static_cast<uint8_t>(state) >= static_cast<uint8_t>(ParseState::InKeyString)
           && static_cast<uint8_t>(state) <= static_cast<uint8_t>(ParseState::AfterValue);
  }

  /**
   * @return true if the character is json whitespace
   */
  static constexpr bool isJsonWhitespace(const char c) noexcept {
    return c == ' ' || c == '\t' || c == '\n' || c == '\r';
  }

  static constexpr ParseState advanceState(ParseState state) noexcept {
    // CORRECTNESS: relies on the unicode states being adjacent.
    return static_cast<ParseState>(static_cast<uint8_t>(state) + 1);
  }

  static constexpr bool isAnyUnicodeState(const ParseState state) noexcept {
    // Is the most significant bit set to 1?
    return (static_cast<uint8_t>(state) & 0x80) != 0;
  }

  static constexpr bool isUnicodeInKeyState(const ParseState state) noexcept {
    // Is the 2nd most significant bit set to 1?
    return (static_cast<uint8_t>(state) & 0x40) == 0;
  }

  static constexpr bool isUnicodeInLowSurrogate(const ParseState state) noexcept {
    // Is the 3rd most significant bit set to 1?
    return (static_cast<uint8_t>(state) & 0x20) != 0;
  }

  /**
   * CORRECTNESS: the state is not checked to be a unicode state.
   */
  static constexpr uint8_t unicodeIndex(const ParseState state) noexcept {
    // Has the unicode state reached the 4th character?
    return static_cast<uint8_t>(state) & 0x0F;
  }

  //***************************************************************************
  // Instance-level util functions.
  //***************************************************************************

  /**
   * Pass to the numerically next state if c is the expected character or set the state as errored and return BadLiteral.
   */
  ParseError advanceWithOr(const char c, const char expected, const ParseError err) {
    if (c == expected) {
      this->state = BaseParser::advanceState(this->state);
      return ParseError::None;
    } else {
      this->state = ParseState::Errored;
      return err;
    }
  }


  //***************************************************************************
  // Instance-level unicode functions.
  //***************************************************************************
#if PJ_ESCAPED_UNICODE == 1

  /**
   * Handles hex characters encoding unicode codepoints.
   *
   * @param c an valid hex character.
   * @param inKey if the parser is inside a key.
   * @param isLowSurrogate if the parser is inside a UTF-16 low surrogate codepoint.
   */
  ParseError dispatchUnicodeHexCharacter(const char c, const bool inKey, const bool isLowSurrogate, const uint8_t unicodeIndex) {
    if (!isHexDigit(c)) {
      return ParseError::BadUnicode;
    }
    this->charBuffer.pushCharUnchecked(c);

    bool invalidUnicodeReplaced = false;


    if (unicodeIndex == 3) {
      // Pointer to the unicode characters
      const uint16_t hexCharactersCount = isLowSurrogate ? 8 : 4;
      auto unicodeBuffer = this->charBuffer.sliceFromEndUnchecked(hexCharactersCount);

      // Modifies the buffer on success.
      const auto convErr = tryConvertInplace(unicodeBuffer, isLowSurrogate);

      switch (convErr.err) {
        case UTF16ConversionError::IncompletePair:
          this->state = inKey ? ParseState::InKeyAfterUnicodeHighSurrogate0 : ParseState::InValueAfterUnicodeHighSurrogate0;
          break;
        case UTF16ConversionError::InvalidCodepoint:
          invalidUnicodeReplaced = true;
        case UTF16ConversionError::None:
          // Adjust the buffer size by retracting it.
          this->charBuffer.popDestroyBy(hexCharactersCount - convErr.written);

          this->state = inKey ? ParseState::InKeyString : ParseState::InValueString;
          break;
      }
    } else {
      this->state = BaseParser::advanceState(this->state);
    }
    return invalidUnicodeReplaced ? ParseError::BadUnicodeCodepoint : ParseError::None;
  }

  /**
   * Is there even a good name for this method?
   * Check that c is the character 'u' (such that the parsed input was the escape sequence '\u'), and reserves 4
   * characters in the buffer.
   */
  ParseError tryPrepareUnicodeLowSurrogate(const char c, const bool inKey) {
    if (c == 'u') {
      // Fail early if there's not enough space for unicode.
      if (!this->charBuffer.ensureAvailable(4)) {
        this->state = ParseState::Errored;
        return ParseError::BufferOverflow;
      }

      this->state = inKey ? ParseState::InKeyUnicodeL0 : ParseState::InValueUnicodeL0;
      return ParseError::None;
    } else {
      this->state = ParseState::Errored;
      return ParseError::BadUnicode;
    }
  }

  ParseError onUnicodeBegin(const bool isKeyString) {
    this->charBuffer.clear();

    // Fail early if there's not enough space for unicode.
    if (!this->charBuffer.ensureAvailable(4)) {
      this->state = ParseState::Errored;
      return ParseError::BufferOverflow;
    }

    this->state = isKeyString ? ParseState::InKeyUnicodeCH0 : ParseState::InValueUnicodeCH0;
    return ParseError::None;
  }

#endif


  //***************************************************************************
  // Dispatch functions.
  //***************************************************************************

  ParseError dispatchBeginDocument(const char c) {
    if (c == '[') {
      return this->onArrayBegin();
    } else if (c == '{') {
      return this->onObjectBegin();
    } else {
      this->state = ParseState::Errored;
      return ParseError::BadDocument;
    }
  }

  ParseError dispatchGenericString(const char c, const bool isKeyString) {
    if (c == '"') {
      if (isKeyString) {
        return this->onKeyStringEnd();
      } else {
        return this->onValueStringEnd();
      }
    } else if (c == '\\') {
      if (isKeyString) {
        this->state = ParseState::InEscapeKeyBegin;
      } else {
        this->state = ParseState::InEscapeValueBegin;
      }
    } else if (c < 0x001f || c == 0x007f) {
      return ParseError::UnescapedControlCharacter;
    } else {
      if (isKeyString) {
        if (!this->charBuffer.pushChar(c)) {
          return ParseError::BufferOverflow;
        }
      } else {
        if (!this->charBuffer.pushOrReplaceChar(c)) {
          return ParseError::StringOverflow;
        }
      }
    }

    return ParseError::None;
  }

  ParseError dispatchEscapeCharacter(const char c, const bool isKeyString) {
    char escapedChar;
    switch (c) {
      case '"':
        escapedChar = '"';
        break;
      case '\\':
        escapedChar = '\\';
        break;
      case '/':
        escapedChar = '/';
        break;
      case 'b':
        escapedChar = '\b';
        break;
      case 'f':
        escapedChar = '\f';
        break;
      case 'n':
        escapedChar = '\n';
        break;
      case 'r':
        escapedChar = '\r';
        break;
      case 't':
        escapedChar = '\t';
        break;
#if PJ_ESCAPED_UNICODE == 1
      case 'u':
        return this->onUnicodeBegin(isKeyString);
#endif
      default:
        this->state = ParseState::Errored;
        return ParseError::BadEscapeCharacter;
    }

    if (!this->charBuffer.pushChar(escapedChar)) {
      this->state = ParseState::Errored;
      return ParseError::BufferOverflow;
    } else {
      this->state = isKeyString ? ParseState::InKeyString : ParseState::InValueString;
      return ParseError::None;
    }
  }

  ParseError dispatchAfterValue(const char c) {
    /*
     * If the end of json structure structure is detected, changing state to
     * InArray or InObject would be more clean structurally speaking, but it's
     * impractical code wise, so shortcut to on[Array,Object]End here.
     */
    if (c == ']') {
      return this->onArrayEnd();
    } else if (c == '}') {
      return this->onObjectEnd();
    } else {
      const auto structure = this->pathStack.peekHead();

      if (c == ',' || BaseParser::isJsonWhitespace(c)) {
        // Still in whatever it's the current structure.
        this->state = structure == JsonStructureType::Object ? ParseState::InObject : ParseState::InArray;
        return ParseError::None;
      } else {
        // Unknown characters bad.
        this->state = ParseState::Errored;
        return structure == JsonStructureType::Object ? ParseError::BadObject : ParseError::BadArray;
      }
    }
  }

  /**
   * To call on the begin of a value.
   * CORRECTNESS: must be called only when parsing a value and not a key. Does not check the current state if it's key or value.
   */
  PJ_FORCE_INLINE ParseError dispatchValue(const char c) {
    if (c == '[') {
      return this->onArrayBegin();
    } else if (c == '{') {
      return this->onObjectBegin();
    } else if (c == '"') {
      // Function MUST be called only on values, so assume it's a value string and not a key's string.
      this->onValueStringBegin();
    } else if (c == 't') {
      this->state = ParseState::InValueTrue1;
    } else if (c == 'f') {
      this->state = ParseState::InValueFalse1;
    } else if (c == 'n') {
      this->state = ParseState::InValueNull1;
    } else if (isDigit(c) || c == '-') {
      return this->onNumberBegin(c);
    } else {
      this->state = ParseState::Errored;
      return ParseError::BadValue;
    }

    return ParseError::None;
  }

  ParseError dispatchInArray(const char c) {
    if (c == ']') {
      return this->onArrayEnd();
    } else {
      return this->dispatchValue(c);
    }
  }

  ParseError dispatchInObject(const char c) {
    if (c == '}') {
      return this->onObjectEnd();
    } else if (c == '"') {
      this->onKeyStringBegin();
      return ParseError::None;
    } else {
      this->state = ParseState::Errored;
      return ParseError::BadKey;
    }
  }

  ParseError dispatchEndKey(const char c) {
    if (c != ':') {
      this->state = ParseState::Errored;
      return ParseError::BadObject;
    } else {
      this->state = ParseState::AfterKey;
      return ParseError::None;
    }
  }


  //***************************************************************************
  // Begin/end functions
  //***************************************************************************

  PJ_FORCE_INLINE ParseError onArrayBegin() {
    return this->onStructureBegin(JsonStructureType::Array, JsonStructure::Array, ParseState::InArray);
  }

  PJ_FORCE_INLINE ParseError onObjectBegin() {
    return this->onStructureBegin(JsonStructureType::Object, JsonStructure::Object, ParseState::InObject);
  }

  ParseError onStructureBegin(
    const JsonStructureType expectedType, const JsonStructure beginningStructure, const ParseState structureState) {
    const uint16_t depth = this->pathStack.size();

    if (!this->pathStack.push(expectedType)) {
      this->state = ParseState::Errored;
      return ParseError::DepthOverflow;
    }

    this->handle.onStructureBegin(beginningStructure, depth);

    this->state = structureState;
    return ParseError::None;
  }

  void onKeyStringBegin() {
    this->charBuffer.clear();
    this->state = ParseState::InKeyString;
  }

  void onValueStringBegin() {
    this->charBuffer.clear();
    this->state = ParseState::InValueString;
  }

  PJ_FORCE_INLINE ParseError onArrayEnd() {
    return this->onStructureEnd(JsonStructureType::Array, JsonStructure::Array, ParseError::BadObject);
  }

  PJ_FORCE_INLINE ParseError onObjectEnd() {
    return this->onStructureEnd(JsonStructureType::Object, JsonStructure::Object, ParseError::BadArray);
  }

  /**
   * @param expectedType: the json structure type expected from the stack.
   * @param endingStructure: the type of the structure that is being ended.
   * @param onTypeError: the error to report if the stack structure is valid but different from expectedType.
   */
  ParseError onStructureEnd(const JsonStructureType expectedType, const JsonStructure endingStructure, const ParseError onTypeError) {
    const auto structure = this->pathStack.pop();

    if (structure == expectedType) {
      const auto depth = this->pathStack.size();
      this->state = depth == 0 ? ParseState::DocumentEnd : ParseState::AfterValue;
      this->handle.onStructureEnd(endingStructure, depth);

      return ParseError::None;
    } else if (structure == JsonStructureType::Invalid) {
      this->state = ParseState::Errored;
      return ParseError::InvalidState;
    } else {
      this->state = ParseState::Errored;
      return onTypeError;
    }
  }

  ParseError onNumberBegin(const char c) {
    this->charBuffer.clear();

    if (!this->charBuffer.pushChar(c)) {
      this->state = ParseState::Errored;
      return ParseError::BufferOverflow;
    } else {
      this->state = ParseState::InValueNumber;
      return ParseError::None;
    }
  }

  ParseError onKeyStringEnd() {
    if (!this->charBuffer.pushChar('\0')) {
      this->state = ParseState::Errored;
      return ParseError::BufferOverflow;
    }

    const char* key = this->charBuffer.rawBuffer();
    this->handle.onKey(key);

    this->state = ParseState::EndKey;

    return ParseError::None;
  }

  ParseError onValueStringEnd() {
    const auto pushed = this->charBuffer.pushOrReplaceChar('\0');

    const auto value = this->charBuffer.rawBuffer();

    const auto type = wellKnownTransmute<bool, JsonValueTypeImpl>(!pushed);
    const auto jsonValue = JsonValueW(type, JsonVariant(value));

    this->handle.onValue(jsonValue);

    this->state = ParseState::AfterValue;

    return pushed ? ParseError::None : ParseError::StringOverflow;
  }

  ParseError onValueBoolEnd(const char c, const char expected, const bool value) {
    if (c == expected) {
      const auto jsonValue = JsonValueW(JsonValueTypeImpl::Bool, JsonVariant(value));

      this->handle.onValue(jsonValue);

      this->state = ParseState::AfterValue;
      return ParseError::None;
    } else {
      this->state = ParseState::Errored;
      return ParseError::BadLiteral;
    }
  }

  ParseError onValueNullEnd(const char c) {
    if (c == 'l') {
      const auto jsonValue = JsonValueW(JsonValueTypeImpl::Null, JsonVariant());
      this->handle.onValue(jsonValue);

      this->state = ParseState::AfterValue;
      return ParseError::None;
    } else {
      this->state = ParseState::Errored;
      return ParseError::BadLiteral;
    }
  }

public:
  constexpr BaseParser() noexcept : state(ParseState::Errored) {}

  explicit BaseParser(
    CharArena_t<char>&& charArena, StackArena_t<uint8_t>&& stackArena, const uint16_t maxDepth, const Handle_t& handle) noexcept
    : charBuffer(move(charArena)), pathStack(move(stackArena), maxDepth), handle(handle) {
    this->reset();
  }

  BaseParser(const BaseParser& other) = delete;
  BaseParser& operator=(const BaseParser& other) = delete;

  BaseParser(BaseParser&& other) noexcept = default;
  BaseParser& operator=(BaseParser&& other) noexcept = default;


  /**
   * Reset the parser to it's initial default state.
   */
  void reset() noexcept {
    this->state = ParseState::DocumentBegin;
    this->pathStack.reset();
    this->charBuffer.reset();
  }

  /**
   * Parse a character.
   * @note The terminator character is always ignored.
   *
   * @returns an error reporting the parser state.
   */
  ParseError parse(const char c) {
    // Is the current state sensible to whitespace?
    const auto whitespaceSensibleState = BaseParser::isParsingTokenState(this->state);

    // Discard whitespace if outside a token and the terminator character.
    if (c == '\0' || (BaseParser::isJsonWhitespace(c) && (!whitespaceSensibleState || this->state == ParseState::DocumentBegin))) {
      return ParseError::None;
    }

    switch (this->state) {
      /**
       * "Special" states.
       */
      case ParseState::DocumentEnd:
      case ParseState::Errored:
        return ParseError::Complete;

      case ParseState::DocumentBegin:
        return this->dispatchBeginDocument(c);

      /**
       * Key parsing.
       */
      case ParseState::InKeyString:
        return this->dispatchGenericString(c, true);

      case ParseState::InEscapeKeyBegin:
        return this->dispatchEscapeCharacter(c, true);

      /**
       * Value parsing.
       */
      case ParseState::InValueTrue1:
        return this->advanceWithOr(c, 'r', ParseError::BadLiteral);

      case ParseState::InValueTrue2:
        return this->advanceWithOr(c, 'u', ParseError::BadLiteral);

      case ParseState::InValueTrue3:
        return this->onValueBoolEnd(c, 'e', true);

      case ParseState::InValueFalse1:
        return this->advanceWithOr(c, 'a', ParseError::BadLiteral);

      case ParseState::InValueFalse2:
        return this->advanceWithOr(c, 'l', ParseError::BadLiteral);

      case ParseState::InValueFalse3:
        return this->advanceWithOr(c, 's', ParseError::BadLiteral);

      case ParseState::InValueFalse4:
        return this->onValueBoolEnd(c, 'e', false);

      case ParseState::InValueNull1:
        return this->advanceWithOr(c, 'u', ParseError::BadLiteral);

      case ParseState::InValueNull2:
        return this->advanceWithOr(c, 'l', ParseError::BadLiteral);

      case ParseState::InValueNull3:
        return this->onValueNullEnd(c);

      case ParseState::InValueString:
        return this->dispatchGenericString(c, false);

      case ParseState::InEscapeValueBegin:
        return this->dispatchEscapeCharacter(c, false);

      case ParseState::InValueNumber:
        // CORRECTNESS: Must be placed immediately before the AfterValue state case, since it falls through to it.
        {
          // Do the minimum checks only to detect the end of the value.
          // The number's formatting and validity is verified by the tryParseNumber function later on.
          if (isNumberCharacter(c)) {
            if (!this->charBuffer.pushChar(c)) {
              this->state = ParseState::Errored;
              return ParseError::BufferOverflow;
            }
            return ParseError::None;
          } else {
            if (!this->charBuffer.pushChar('\0')) {
              this->state = ParseState::Errored;
              return ParseError::BufferOverflow;
            }

            const auto str = this->charBuffer.rawBuffer();
            const auto parseRes = tryParseNumber(str);

            JsonValueW jsonValue;
            if (parseRes.err == NumParseError::Invalid) {
              this->state = ParseState::Errored;
              return ParseError::BadNumber;
            } else {
#if PJ_INTEGER_ONLY_NUM_PARSE == 0
              if (parseRes.err == NumParseError::IntegerNone || parseRes.err == NumParseError::IntegerOverflow) {
                // SAFETY: both enumerations use WellKnown values.
                const auto type = wellKnownTransmute<NumParseError, JsonValueTypeImpl>(parseRes.err);
                jsonValue = JsonValueW(type, JsonVariant(parseRes.iValue));
              } else {
                // SAFETY: both enumerations use WellKnown values.
                const auto type = wellKnownTransmute<NumParseError, JsonValueTypeImpl>(parseRes.err);
                jsonValue = JsonValueW(type, JsonVariant(parseRes.fValue));
              }
#else
              // SAFETY: both enumerations use WellKnown values.
              const auto type = wellKnownTransmute<NumParseError, JsonValueTypeImpl>(parseRes.err);
              jsonValue = JsonValueW(type, JsonVariant(parseRes.iValue));
#endif
            }

            this->handle.onValue(const_cast<const JsonValueW&>(jsonValue));

            this->state = ParseState::AfterValue;
            // The current character isn't part of the number. Fall through to the AfterValue state.
          }
        }
        // Intentionally without break to fall through to the AfterValue state.

      case ParseState::AfterValue:
        return this->dispatchAfterValue(c);

      case ParseState::InArray:
        return this->dispatchInArray(c);

      case ParseState::InObject:
        return this->dispatchInObject(c);

      case ParseState::AfterKey:
        return this->dispatchValue(c);

      case ParseState::EndKey:
        return this->dispatchEndKey(c);

#if PJ_ESCAPED_UNICODE == 1
      case ParseState::InKeyAfterUnicodeHighSurrogate0:
      case ParseState::InValueAfterUnicodeHighSurrogate0:
        return this->advanceWithOr(c, '\\', ParseError::BadUnicode);

      case ParseState::InKeyAfterUnicodeHighSurrogate1:
        return this->tryPrepareUnicodeLowSurrogate(c, true);

      case ParseState::InValueAfterUnicodeHighSurrogate1:
        return this->tryPrepareUnicodeLowSurrogate(c, false);

      default:
        // Decode the unicode state from here.
        {

          if (BaseParser::isAnyUnicodeState(this->state)) {
            const auto inKey = BaseParser::isUnicodeInKeyState(this->state);
            const auto isLowSurrogate = BaseParser::isUnicodeInLowSurrogate(this->state);
            const auto unicodeIndex = BaseParser::unicodeIndex(this->state);

            return this->dispatchUnicodeHexCharacter(c, inKey, isLowSurrogate, unicodeIndex);
          }

          this->state = ParseState::Errored;
          return ParseError::InvalidState;
        }
#else
      default:
        this->state = ParseState::Errored;
        return ParseError::InvalidState;
#endif
    }
  }
};

}

#endif
