#ifndef JSON_STRUCTURE_TYPE_H
#define JSON_STRUCTURE_TYPE_H

#include <stdint.h>

#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

enum class JsonStructureType : uint8_t {
  Invalid,
  Object,
  Array
};

}

#endif
