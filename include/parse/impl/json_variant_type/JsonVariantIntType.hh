#ifndef JSON_VARIANT_INT_TYPE_H
#define JSON_VARIANT_INT_TYPE_H

#if PJ_INTEGER_ONLY_NUM_PARSE == 1

#include <stdint.h>

#include <util/Namespace.hh>
#include <util/WellKnown.hh>


namespace PJ_IMPL_NAMESPACE {

enum class JsonValueTypeImpl : uint8_t {
  String = WellKnown::StringErrNone,
  StringTruncated = WellKnown::StringTruncated,
  Integer = WellKnown::IntegerErrNone,
  IntegerOverflow = WellKnown::IntegerOverflow,
  Bool,
  Null
};

union JsonVariant {
  const char* stringValue;
  int32_t integerValue;
  bool boolValue;

  inline explicit JsonVariant() {}
  constexpr explicit JsonVariant(const char* stringValue) noexcept : stringValue(stringValue) {}
  constexpr explicit JsonVariant(const int32_t integerValue) noexcept : integerValue(integerValue) {}
  constexpr explicit JsonVariant(const bool boolValue) noexcept : boolValue(boolValue) {}
};

}

#endif

#endif
