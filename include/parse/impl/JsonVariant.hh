#ifndef JSON_VARIANT_H
#define JSON_VARIANT_H

#if PJ_INTEGER_ONLY_NUM_PARSE == 1
#include <parse/impl/json_variant_type/JsonVariantIntType.hh>
#else
#include <parse/impl/json_variant_type/JsonVariantType.hh>
#endif

#endif
