#ifndef SLAB_PARSER_H
#define SLAB_PARSER_H

#include <stdint.h>
#include <stdlib.h>
#include <limits.h>

#include <parse/ParseError.hh>

#include <parse/impl/BaseParser.hh>
#include <parse/impl/CharBuffer.hh>
#include <parse/impl/PathStack.hh>

#include <util/arena/SlabArena.hh>
#include <util/BytesBits.hh>
#include <util/Polyfills.hh>
#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

/**
 * A json parser with a heap allocated buffer.
 * The single allocation is never resized in order to prevent fragmentation.
 */
template<typename H> class SlabParser {
private:
  template<typename T> using PCArena = SlabArena<T>;
  template<typename T> using PSArena = SlabArena<T>;
  using SBaseParser = BaseParser<PCArena, PSArena, H>;

  SBaseParser impl;


  // The only way to spare the consumer to have to implement a default constructor for the handle
  static SBaseParser allocParser(const uint16_t charBufferSize, const uint16_t maxDocumentDepth, const H& handle) noexcept {
    // SlabStackPath uses a bitset. Calculate the number of bytes required in the buffer to host maxDepth bits.
    auto stackByteSize = bytesForBitset(maxDocumentDepth);

#if CHAR_BIT == 8
    auto buffer = reinterpret_cast<uint8_t*>(malloc((charBufferSize + stackByteSize) * sizeof(uint8_t)));

    if (buffer != nullptr) {
      auto charBuffer = reinterpret_cast<char*>(buffer);
      auto stackBuffer = buffer + charBufferSize;

      return SBaseParser(
        move(PCArena<char>(charBuffer, charBufferSize)), move(PSArena<uint8_t>(stackBuffer, stackByteSize)), maxDocumentDepth, handle);
    } else {
      return SBaseParser(move(PCArena<char>(nullptr, 0)), move(PSArena<uint8_t>(nullptr, 0)), 0, handle);
    }
#else
#error "Unsupported platform. Implement proper allocation logic"
#endif
  }

  void freeParser() noexcept {
#if CHAR_BIT == 8
    auto buffer = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(this->impl.charBuffer.rawBuffer()));
    free(buffer);
#else
#error "Unsupported platform. Implement proper deallocation logic"
#endif
  }

public:
  constexpr explicit SlabParser(const uint16_t charBufferSize, const uint16_t maxDocumentDepth, const H& handle) noexcept
    : impl(move(SlabParser::allocParser(charBufferSize, maxDocumentDepth, handle))) {}

  ~SlabParser() {
    this->freeParser();
  }

  // Non-copyable
  SlabParser(const SlabParser& other) = delete;
  SlabParser& operator=(const SlabParser& other) = delete;

  // Movable
  SlabParser(SlabParser&& other) noexcept = default;
  SlabParser& operator=(SlabParser&& other) noexcept = default;


  /**
   * Advances the parser state with the given character. The terminator character is always ignored.
   * @return an error reporting the parser's state.
   */
  ParseError parse(const char c) {
    return this->impl.parse(c);
  }

  /**
   * Reset the parser to it's initial default state.
   */
  void reset() noexcept {
    this->impl.reset();
  }

  H& handle() noexcept {
    return this->impl.handle;
  }
};

}

#endif
