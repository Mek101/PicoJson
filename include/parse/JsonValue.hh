#ifndef JSON_VALUE_H
#define JSON_VALUE_H

#include <stdint.h>

#include <parse/impl/JsonVariant.hh>

#include <util/Namespace.hh>
#include <util/Polyfills.hh>
#include <util/WellKnown.hh>


namespace PJ_IMPL_NAMESPACE {

#if PJ_INTEGER_ONLY_NUM_PARSE == 1

enum class JsonValueType : uint8_t {
  String,
  Integer,
  Bool,
  Null
};

#else

enum class JsonValueType : uint8_t {
  String,
  Float,
  Integer,
  Bool,
  Null
};

#endif


/**
 * A json value variant.
 */
class JsonValue {
protected:
  JsonVariant variantData;
  JsonValueTypeImpl type;


  inline JsonValue() noexcept {}

  constexpr JsonValue(const JsonValueTypeImpl type, const JsonVariant variant) noexcept
    : variantData(variant), type(type) {}

#if PJ_INTEGER_ONLY_NUM_PARSE == 1
  inline JsonValueType asType() const noexcept {
    switch (this->type) {
      case JsonValueTypeImpl::String:
      case JsonValueTypeImpl::StringTruncated:
        return JsonValueType::String;
      case JsonValueTypeImpl::Integer:
      case JsonValueTypeImpl::IntegerOverflow:
        return JsonValueType::Integer;
      case JsonValueTypeImpl::Bool:
        return JsonValueType::Bool;
      default:
        return JsonValueType::Null;
    }
  }
#else
  inline JsonValueType asType() const noexcept {
    switch (this->type) {
      case JsonValueTypeImpl::String:
      case JsonValueTypeImpl::StringTruncated:
        return JsonValueType::String;
      case JsonValueTypeImpl::Float:
      case JsonValueTypeImpl::FloatOverflow:
      case JsonValueTypeImpl::FloatUnderflow:
        return JsonValueType::Float;
      case JsonValueTypeImpl::Integer:
      case JsonValueTypeImpl::IntegerOverflow:
        return JsonValueType::Integer;
      case JsonValueTypeImpl::Bool:
        return JsonValueType::Bool;
      default:
        return JsonValueType::Null;
    }
  }
#endif

public:
  JsonValue(const JsonValue& other) noexcept = default;
  JsonValue& operator=(JsonValue& other) noexcept = default;

  JsonValue(JsonValue&& other) noexcept = default;
  JsonValue& operator=(JsonValue&& other) noexcept = default;


  /**
   * @return the type of the value.
   */
  inline JsonValueType getValueType() const noexcept {
    return this->asType();
  }

  /**
   * SAFETY: the caller must ensure the value is a string.
   * 
   * @return a pointer to a c-style string.
   */
  inline const char* getStringUnchecked() const noexcept {
    return this->variantData.stringValue;
  }

#if PJ_INTEGER_ONLY_NUM_PARSE == 0
  /**
    * SAFETY: the caller must ensure the value is a floating point number.
    * 
    * @return a floating point number.
    */
  inline fnat_t getFloatUnchecked() const noexcept {
    return this->variantData.fnatValue;
  }
#endif

  /**
   * SAFETY: the caller must ensure the value is a 32-bit integer number.
   * 
   * @return a 32-bit integer number.
   */
  inline int32_t getIntegerUnchecked() const noexcept {
    return this->variantData.integerValue;
  }

  /**
   * SAFETY: the caller must ensure the value is a boolean.
   * 
   * @return a boolean.
   */
  inline bool getBoolUnchecked() const noexcept {
    return this->variantData.boolValue;
  }

  /**
   * @return true if the value is a c-style string which was truncated.
   */
  inline bool isStringTruncated() const noexcept {
    return this->type == JsonValueTypeImpl::StringTruncated;
  }

  /**
   * @return true if the value is a 32-bit integer number which overflowed.
   */
  inline bool isIntegerOverflow() const noexcept {
    return this->type == JsonValueTypeImpl::IntegerOverflow;
  }

#if PJ_INTEGER_ONLY_NUM_PARSE == 0
  /**
   * @return true if the value is a floating point number which overflowed.
   */
  inline bool isFloatOverflow() const noexcept {
    return this->type == JsonValueTypeImpl::FloatOverflow;
  }

  /**
   * @return true if the value is a floating point number which underflowed.
   */
  inline bool isFloatUnderflow() const noexcept {
    return this->type == JsonValueTypeImpl::FloatUnderflow;
  }
#endif
};

}

#endif
