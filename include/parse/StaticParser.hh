#ifndef STATIC_PARSER_H
#define STATIC_PARSER_H

#include <stdint.h>

#include <parse/ParseError.hh>

#include <parse/impl/BaseParser.hh>
#include <parse/impl/CharBuffer.hh>
#include <parse/impl/PathStack.hh>

#include <util/arena/StaticArena.hh>
#include <util/BytesBits.hh>
#include <util/Polyfills.hh>
#include <util/Namespace.hh>


namespace PJ_IMPL_NAMESPACE {

/**
 * A json parser with a statically allocated inline buffer.
 * The inline buffer should make the parser the most performant, but also the slowest to copy and move.
 */
template<uint16_t CHAR_BUFFER_SIZE, uint16_t MAX_DOCUMENT_DEPTH, typename H> class StaticParser {
private:
  template<typename T> using PCArena = StaticArena<T, CHAR_BUFFER_SIZE>;
  template<typename T> using PSArena = StaticArena<T, bytesForBitset(MAX_DOCUMENT_DEPTH)>;
  using SBaseParser = BaseParser<PCArena, PSArena, H>;

  SBaseParser impl;
public:
  constexpr explicit StaticParser(const H& handle) noexcept : impl(move(PCArena<char>()), move(PSArena<uint8_t>()), MAX_DOCUMENT_DEPTH, handle) {}

  // Non-copyable
  StaticParser(const StaticParser& other) = delete;
  StaticParser& operator=(const StaticParser& other) = delete;

  // Movable
  StaticParser(StaticParser&& other) noexcept = default;
  StaticParser& operator=(StaticParser&& other) noexcept = default;


  /**
   * Advances the parser state with the given character. The terminator character is always ignored.
   * @return an error reporting the parser's state.
   */
  ParseError parse(const char c) {
    return this->impl.parse(c);
  }

  /**
   * Reset the parser to it's initial default state.
   */
  void reset() noexcept {
    this->impl.reset();
  }

  H& handle() noexcept {
    return this->impl.handle;
  }
};

}

#endif
