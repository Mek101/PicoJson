# PicoJson

[![status-badge](https://ci.codeberg.org/api/badges/Mek101/PicoJson/status.svg)](https://ci.codeberg.org/Mek101/PicoJson)
[![License: MPL 2.0](https://img.shields.io/badge/License-MPL_2.0-brightgreen.svg)](https://opensource.org/licenses/MPL-2.0)
[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

A Json parser for those who should use a binary format instead and the mentally insane.

## What is it

PicoJson is a json parsing library primarily aimed at low-memory embedded environments. To keep memory usage down, PicoJson
uses a streaming parser approach: instead of a single parse method receiving a string and producing an object representing
the serialized json, PicoJson accepts a handle which will receive information about the parsed json as events.

## Usage guidelines

PicoJson proudly uses only 2 buffers (with configurable size) to keep memory usage as low as possible, and as such the
exposed handle API is minimalist, holds no state and exposes very little to the consumer code.
Instead of producing an intermediate DOM object to access Json data, the consumer code should implement the handle as a serializer reacting to events following the state machine pattern.
