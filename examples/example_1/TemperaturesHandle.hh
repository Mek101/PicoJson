#ifndef TEMPERATURES_HANDLE_H
#define TEMPERATURES_HANDLE_H

#include <stdint.h>

#include <PicoJson.hh>


class TemperaturesHandle {
private:
  enum class State {
    Uninit,
    InArray,
    Error,
    Complete,
  };

  State state;

  uint32_t maxTemps;
  uint32_t base;
  uint32_t next;

  float* tempBuffer;


  void pushValue(const float value);
  uint32_t size();
public:
  explicit TemperaturesHandle(const uint32_t maxTemps);
  ~TemperaturesHandle();

  TemperaturesHandle(const TemperaturesHandle& other);
  TemperaturesHandle& operator=(const TemperaturesHandle& other);

  void onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth);
  void onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth);
  void onKey(const char* const key);
  void onValue(const pj::JsonValue& value);

  float average();

  bool isErrored();
  bool isComplete();
};

#endif