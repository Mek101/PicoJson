#include "TemperaturesHandle.hh"

#include <stdio.h>

#include <PicoJson.hh>


const char doc[] = R"([
  13,
  13.43,
  22.8,
  16.87,
  16.1,
  18.331,
  19.4,
  21,
  25.8,
  24,
  24.9,
  26.22,
  17.121,
  18
])";


int main() {
  pj::SlabParser<TemperaturesHandle> parser(8, 1, TemperaturesHandle(10));

  for (auto i = 0; i < sizeof(doc); i++) {
    auto err = parser.parse(doc[i]);

    if (err != pj::ParseError::None) {
      printf("Parser error at %c, %i of %lu characters\n", doc[i], i, sizeof(doc));
      return -1;
    }
    if (parser.handle().isErrored()) {
      printf("Handle error at %c, %i of %lu characters\n", doc[i], i, sizeof(doc));
      return -2;
    }
  }

  if (!parser.handle().isComplete()) {
    printf("Parsing incomplete!");
    return -3;
  }

  printf("%.2f\n", parser.handle().average());

  return 0;
}
