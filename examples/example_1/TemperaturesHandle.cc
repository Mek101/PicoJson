#include "TemperaturesHandle.hh"

#include <stdlib.h>
#include <string.h>


uint32_t TemperaturesHandle::size() {
  if (this->next >= this->base) {
    return this->next - this->base;
  } else {
    const uint32_t cap = this->maxTemps + 1;
    return cap + this->next - this->base;
  }
}

void TemperaturesHandle::pushValue(const float value) {
  if (this->maxTemps == 0) {
    return;
  }

  const uint32_t capacity = this->maxTemps + 1;

  // If the buffer is full, push the base forward and overwrite the oldest value.
  if (this->size() >= capacity - 1) {
    this->base++;
    this->base %= capacity;
  }

  this->tempBuffer[this->next] = value;
  this->next++;
  this->next %= capacity;
}


TemperaturesHandle::TemperaturesHandle(const uint32_t maxTemperatures) : maxTemps(maxTemperatures), base(0), next(0) {
  if (maxTemperatures != 0) {
    this->tempBuffer = static_cast<float*>(calloc(maxTemperatures + 1, sizeof(float)));
    this->state = tempBuffer == nullptr ? State::Error : State::Uninit;
  } else {
    this->state = State::Complete;
  }
}

TemperaturesHandle::~TemperaturesHandle() {
  free(this->tempBuffer);
}

TemperaturesHandle::TemperaturesHandle(const TemperaturesHandle& other)
  : maxTemps(other.maxTemps), base(other.base), next(other.next) {
  if (other.maxTemps != 0) {
    tempBuffer = static_cast<float*>(malloc((maxTemps + 1) * sizeof(float)));
    this->state = tempBuffer == nullptr ? State::Error : other.state;

    memcpy(this->tempBuffer, other.tempBuffer, other.maxTemps);
  } else {
    this->state = State::Complete;
  }
}

TemperaturesHandle& TemperaturesHandle::operator=(const TemperaturesHandle& other) {
  this->maxTemps = other.maxTemps;
  this->base = other.base;
  this->next = other.next;

  if (other.maxTemps != 0) {
    tempBuffer = static_cast<float*>(malloc((maxTemps + 1) * sizeof(float)));
    this->state = tempBuffer == nullptr ? State::Error : other.state;

    memcpy(this->tempBuffer, other.tempBuffer, other.maxTemps);
  } else {
    this->state = State::Complete;
  }

  return *this;
}


void TemperaturesHandle::onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth) {
  if (depth == 0 && structureType == pj::JsonStructure::Array && this->state == State::Uninit) {
    this->state = State::InArray;
  } else {
    this->state = State::Error;
  }
}

void TemperaturesHandle::onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth) {
  if (depth == 0 && this->state == State::InArray) {
    this->state = State::Complete;
  } else {
    this->state = State::Error;
  }
}

void TemperaturesHandle::onKey(const char* key) {
  this->state = State::Error;
}

void TemperaturesHandle::onValue(const pj::JsonValue& value) {
  if (this->state == State::InArray) {
    if (value.getValueType() == pj::JsonValueType::Float) {
      this->pushValue(value.getFloatUnchecked());
    } else if (value.getValueType() == pj::JsonValueType::Integer) {
      this->pushValue(value.getIntegerUnchecked());
    } else {
      this->state = State::Error;
    }
  } else {
    this->state = State::Error;
  }
}

float TemperaturesHandle::average() {
  if (this->maxTemps == 0 || this->size() == 0) {
    return 0.0f;
  }

  float acc = 0.0f;
  for (auto i = this->base; i != this->next; i = (i + 1) % this->size()) {
    acc += this->tempBuffer[i];
  }
  return acc / this->size();
}

bool TemperaturesHandle::isErrored() {
  return this->state == State::Error;
}

bool TemperaturesHandle::isComplete() {
  return this->state == State::Complete;
}
