#ifndef ARTICLES_HANDLE_H
#define ARTICLES_HANDLE_H

#include <stdint.h>

#include <PicoJson.hh>

#define TOKEN_STR_SIZE 8


struct Article {
  int32_t id;
  const char* title;
};


class ArticlesHandle {
private:
  enum class State {
    Uninit,
    InRootObject,
    InTokenKey,
    InArticlesKey,
    InArticlesArray,
    InArticleObject,
    InArticleObjectId,
    InArticleObjectTitle,
    Error,
    Complete
  };

  State state;

  Article *const articles;
  uint16_t capacity;
  uint16_t filled;
  char tokenBuff[TOKEN_STR_SIZE + 1];

  bool hasToken;
  bool hasId;
  bool hasTitle;


  const char* stateToStr(const State state);

  State onId(const int32_t id);
  State onTitle(const char* title);
  State onToken(const char* token);
public:
  explicit ArticlesHandle(Article *const articles, const uint16_t capacity);


  void onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth);

  void onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth);

  void onKey(const char* const key);

  void onValue(const pj::JsonValue& value);


  uint16_t count();

  Article *const items();

  const char* token();

  bool isComplete();
  bool isErrored();
};

#endif