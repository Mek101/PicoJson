#include "ArticlesHandle.hh"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define orError(condition, state) ((condition) ? (state) : State::Error)


enum class Key {
  Unknown,
  Token,
  Articles,
  Id,
  Title,
};

const char* stateStr[] = {
  "Uninit",
  "InRootObject",
  "InTokenKey",
  "InArticlesKey",
  "InArticlesArray",
  "InArticleObject",
  "InArticleObjectId",
  "InArticleObjectTitle",
  "Error",
  "Complete"
};

static Key fromString(const char* str) {
  if (strcmp(str, "id") == 0) {
    return Key::Id;
  } else if (strcmp(str, "title") == 0) {
    return Key::Title;
  } else if (strcmp(str, "token") == 0) {
    return Key::Token;
  } else if (strcmp(str, "articles") == 0) {
    return Key::Articles;
  } else {
    return Key::Unknown;
  }
}


ArticlesHandle::State ArticlesHandle::onId(const int32_t id) {
  auto index = this->filled;

  this->articles[index].id = id;
  this->hasId = true;

  return State::InArticleObject;
}

ArticlesHandle::State ArticlesHandle::onTitle(const char* title) {
  auto index = this->filled;
  auto strLength = strlen(title);
  auto strBufferSize = strLength + 1;

  auto strBuffer = static_cast<char*>(malloc(strBufferSize));
  strcpy(strBuffer, title);

  this->articles[index].title = strBuffer;
  this->hasTitle = true;

  return State::InArticleObject;
}

ArticlesHandle::State ArticlesHandle::onToken(const char* token) {
  auto index = this->filled;
  auto strLength = strlen(token);

  // Tokens must be 8 characters wide.
  if (strLength != TOKEN_STR_SIZE) {
    return State::Error;
  }

  strcpy(this->tokenBuff, token);
  this->hasToken = true;

  return State::InRootObject;
}


ArticlesHandle::ArticlesHandle(Article* const articles, const uint16_t capacity)
  : state(State::Uninit), articles(articles), capacity(capacity), filled(0), hasToken(false), hasId(false), hasTitle(false) {
  this->tokenBuff[0] = '\0';
}

void ArticlesHandle::onKey(const char* const key) {
  const auto enumKey = fromString(key);

  switch (enumKey) {
    case Key::Id:
      this->state = orError(this->state == State::InArticleObject, State::InArticleObjectId);
      return;
    case Key::Title:
      this->state = orError(this->state == State::InArticleObject, State::InArticleObjectTitle);
      return;
    case Key::Token:
      this->state = orError(this->state == State::InRootObject && !this->hasToken, State::InTokenKey);
      return;
    case Key::Articles:
      this->state = orError(this->state == State::InRootObject && this->filled == 0, State::InArticlesKey);
      return;
    default:
      this->state = State::Error;
      return;
  }
}

void ArticlesHandle::onValue(const pj::JsonValue& value) {
  const auto type = value.getValueType();

  switch (type) {
    case pj::JsonValueType::Integer:
      if (this->state == State::InArticleObjectId && !this->hasId) {
        this->state = this->onId(value.getIntegerUnchecked());
      }
      else {
        this->state = State::Error;
      }
      return;
    case pj::JsonValueType::String:
      if (this->state == State::InArticleObjectTitle && !this->hasTitle) {
        this->state = this->onTitle(value.getStringUnchecked());
      } else if (this->state == State::InTokenKey && !this->hasToken) {
        this->state = this->onToken(value.getStringUnchecked());
      } else {
        this->state = State::Error;
      }
      return;
    default:
      this->state = State::Error;
      return;
  }
}

void ArticlesHandle::onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth) {
  auto to = this->state;

  if (depth == 0 && structureType == pj::JsonStructure::Object && this->state == State::Uninit) {
    this->state = State::InRootObject;
  } else if (depth != 0 && structureType == pj::JsonStructure::Array && this->state == State::InArticlesKey) {
    this->state = State::InArticlesArray;
  } else if (depth != 0 && structureType == pj::JsonStructure::Object && this->state == State::InArticlesArray) {
    // Prevent buffer overflow.
    if (this->filled >= this->capacity) {
      this->state = State::Error;
    } else {
      this->state = State::InArticleObject;
    }
  } else {
    this->state = State::Error;
  }
}

void ArticlesHandle::onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth) {
  auto from = this->state;

  switch (this->state) {
    case State::InArticleObject:
      if (this->hasId && this->hasTitle) {
        this->filled++;

        this->hasId = false;
        this->hasTitle = false;
        this->state = State::InArticlesArray;
      }
      else {
        this->state = State::Error;
      }
      break;
    case State::InArticlesArray:
      this->state = State::InRootObject;
      break;
    case State::InRootObject:
      if (this->hasToken) {
        this->state = State::Complete;
      } else {
        this->state = State::Error;
      }
      break;
    default:
      this->state = State::Error;
      break;
  }
}


bool ArticlesHandle::isComplete() {
  return this->state == State::Complete;
}

bool ArticlesHandle::isErrored() {
  return this->state == State::Error;
}

uint16_t ArticlesHandle::count() {
  return this->filled;
}

Article* const ArticlesHandle::items() {
  return this->articles;
}

const char* ArticlesHandle::token() {
  return this->tokenBuff;
}
