#include "ArticlesHandle.hh"

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include <parse/StaticParser.hh>

#define MAX_ARTICLES 8


const char doc[] = R"({
  "articles": [
    {
      "id": 456,
      "title": "PicoJson: the best parser ever!"
    },
    {
      "id": 467,
      "title": "Static typing best typing"
    },
    {
      "id": 7785,
      "title": "CMake: not even once"
    }
  ],
  "token": "Ab2E78j6"
})";


const char *const errStrings[] = {
  "None",
  "StringOverflow",
  "UnescapedControlCharacter",
  "BadDocument",
  "BadKey",
  "BadValue",
  "BadObject",
  "BadArray",
  "BadNumber",
  "BadLiteral",
  "BadEscapeCharacter",
  "BadUnicode",
  "Complete",
  "InvalidState",
  "BufferOverflow",
  "DepthOverflow",
};


const char *const errorToString(const pj::ParseError error) {
  return errStrings[static_cast<uint8_t>(error)];
}

int main() {
  Article articleBuffer[MAX_ARTICLES];
  auto parser = pj::StaticParser<50, 3, ArticlesHandle>(ArticlesHandle(articleBuffer, MAX_ARTICLES));
  ArticlesHandle& handle = parser.handle();

  for (auto i = 0; i < sizeof(doc); i++) {
    auto err = parser.parse(doc[i]);

    if (err != pj::ParseError::None) {
      printf("Error %s!\n", errorToString(err));
      return 1;
    }
    if (handle.isErrored()) {
      printf("Handle error! Count %d\n", handle.count());
      return 2;
    }
  }

  auto items = handle.items();
  for (auto i = 0; i < handle.count(); i++) {
    printf("\n");
    printf("TITLE: %s\n", items[i].title);
    printf("ID: %" PRId32 "\n", items[i].id);
  }

  for (auto i = 0; i < handle.count(); i++) {
    free(const_cast<char*>(items[i].title));
  }

  return 0;
}
