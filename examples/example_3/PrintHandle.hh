#ifndef PRINT_HANDLE_H
#define PRINT_HANDLE_H

#include <PicoJson.hh>


class PrintHandle {
private:
  uint16_t nesting;
public:
  PrintHandle();

  void onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth);
  void onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth);
  void onKey(const char* const key);
  void onValue(const pj::JsonValue& value);
};

#endif
