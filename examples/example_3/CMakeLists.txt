add_executable(Example_3
    ${CMAKE_CURRENT_SOURCE_DIR}/main.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/ObjectFilterHandle.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/PrintHandle.cc
)

target_link_libraries(Example_3
    ${PROJECT_NAME}
)
