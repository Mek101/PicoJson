#include "ObjectFilterHandle.hh"

#include <stdlib.h>
#include <string.h>

#include <stdio.h>


static char* copyKey(const char* src) {
  if (src == nullptr) {
    return nullptr;
  }

  const auto strLen = strlen(src);
  if (strLen == 0) {
    return nullptr;
  }

  const auto buffLen = strLen + 1;
  auto buff = reinterpret_cast<char*>(malloc(buffLen));
  memcpy(buff, src, buffLen);

  return buff;
}


ObjectFilterHandle::ObjectFilterHandle(const char* key, const PrintHandle& handle)
  : nesting(0), state(FoundState::None), handle(handle), key(copyKey(key)) {}

ObjectFilterHandle::~ObjectFilterHandle() {
  free(this->key);
}

ObjectFilterHandle::ObjectFilterHandle(const ObjectFilterHandle& other)
  : nesting(other.nesting), state(other.state), handle(other.handle), key(copyKey(other.key)) {}

ObjectFilterHandle& ObjectFilterHandle::operator=(const ObjectFilterHandle& other) {
  this->nesting = other.nesting;
  this->state = other.state;
  this->handle = other.handle;
  free(this->key);
  this->key = copyKey(other.key);
  return *this;
}


void ObjectFilterHandle::onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth) {
  if (this->state == FoundState::FoundObject) {
    // Forward.
    this->handle.onStructureBegin(structureType, depth - this->nesting);
  } else if (this->state == FoundState::WaitObjectBegin) {
    // Found the object with the key.
    this->state = FoundState::FoundObject;
    this->nesting = depth;
    this->handle.onStructureBegin(structureType, depth - this->nesting);
  }
}

void ObjectFilterHandle::onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth) {
  if (this->state == FoundState::FoundObject) {
    // Forward.
    this->handle.onStructureEnd(structureType, depth - this->nesting);

    // If we reached the end of the filtered object, unfind it.
    if (this->nesting == depth) {
      this->state = FoundState::None;
    }
  }
}

void ObjectFilterHandle::onKey(const char* const key) {
  if (this->state == FoundState::FoundObject) {
    this->handle.onKey(key);
  } else if (this->state == FoundState::None) {
    if (this->key != nullptr && strcmp(key, this->key) == 0) {
      // Found the key! Wait for the object to begin!
      this->state = FoundState::WaitObjectBegin;
    }
  }
}

void ObjectFilterHandle::onValue(const pj::JsonValue& value) {
  if (this->state == FoundState::FoundObject) {
    // Forward.
    this->handle.onValue(value);
  } else if(this->state == FoundState::WaitObjectBegin) {
    // The key didn't have an object as a value, reset the state.
    this->state = FoundState::None;
  }
}
