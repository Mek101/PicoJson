#include "PrintHandle.hh"

#include <stdio.h>
#include <inttypes.h>


static void nestBy(const uint16_t depth) {
  for (auto i = 0; i < depth; i++) {
    putchar('\t');
  }
}


PrintHandle::PrintHandle() : nesting(0) {}

void PrintHandle::onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth) {
  this->nesting = depth;
  if (this->nesting != 0) {
    putchar('\n');
  }
}

void PrintHandle::onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth) {
  this->nesting = depth;
}

void PrintHandle::onKey(const char* const key) {
  nestBy(this->nesting);
  printf("\"%s\": ", key);
}

void PrintHandle::onValue(const pj::JsonValue& value) {
  switch (value.getValueType()) {
    case pj::JsonValueType::String:
      printf("\"%s\"\n", value.getStringUnchecked());
      break;
    case pj::JsonValueType::Integer:
      printf("%" PRId32 "\n", value.getIntegerUnchecked());
      break;
    case pj::JsonValueType::Float:
      printf("%.6f\n", value.getFloatUnchecked());
      break;
    case pj::JsonValueType::Bool:
      printf("%s\n", value.getBoolUnchecked() ? "true" : "false");
      break;
    case pj::JsonValueType::Null:
    default:
      printf("null\n");
      break;
  }
}
