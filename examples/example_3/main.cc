#include "ObjectFilterHandle.hh"
#include "PrintHandle.hh"

#include <PicoJson.h>


const char doc[] = R"({
  "hidden1": {
    "hidden2": {
      "seen": {
        "key1": 1,
        "other": {
          "key_nest": 3.14
        },
        "key2": "cool"
      },
      "unkey": 12
    },
    "seen": {
      "key123": false,
      "key321": null
    }
  }
})";


int main() {
  pj::SlabParser<ObjectFilterHandle> parser(20, 8, ObjectFilterHandle("seen", PrintHandle()));

  for (auto i = 0; i < sizeof(doc); i++) {
    const auto err = parser.parse(doc[i]);

    if (err != pj::ParseError::None) {
      return 1;
    }
  }

  return 0;
}
