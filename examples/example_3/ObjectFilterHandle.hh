#ifndef OBJECT_FILTER_HANDLE_H
#define OBJECT_FILTER_HANDLE_H

#include <stdint.h>

#include <PicoJson.hh>

#include "PrintHandle.hh"


class ObjectFilterHandle {
private:
  enum class FoundState {
    None,
    WaitObjectBegin,
    FoundObject
  };

  char* key;
  FoundState state;
  uint16_t nesting;

  PrintHandle handle;
public:
  explicit ObjectFilterHandle(const char* key, const PrintHandle& handle);
  ~ObjectFilterHandle();

  ObjectFilterHandle(const ObjectFilterHandle& other);
  ObjectFilterHandle& operator=(const ObjectFilterHandle& other);

  void onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth);
  void onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth);
  void onKey(const char* const key);
  void onValue(const pj::JsonValue& value);
};

#endif
