#include "ArenaUtil.hh"

#include <string.h>

#include <unity.h>

#include <util/arena/DynArena.hh>

template<typename T, uint16_t M> using DynArena = PJ_IMPL_NAMESPACE::DynArena<T, M>;


static void test_simpleArena() {
  const auto maxSize = 20;
  auto arena = DynArena<char, 4>(maxSize);

  TEST_ASSERT_MESSAGE(arena.capacity() == maxSize, "Incorrect capacity");


  // Does it respect the given minimum allocation?
  TEST_ASSERT(arena.setAvailable(2));
  TEST_ASSERT(arena.available() == 4);

  TEST_ASSERT(arena.rawBuffer() != nullptr);


  // Available space doesn't go beyond maximum size given.
  TEST_ASSERT_FALSE(arena.setAvailable(200));
  // Available area isn't altered by a bogus size request.
  TEST_ASSERT(arena.available() == 4);

  TEST_ASSERT(arena.rawBuffer() != nullptr);


  // Does it reach max size?
  TEST_ASSERT(arena.setAvailable(maxSize));
  TEST_ASSERT(arena.available() == maxSize);

  TEST_ASSERT(arena.rawBuffer() != nullptr);


  TEST_ASSERT(arena.setAvailable(maxSize / 3));
  TEST_ASSERT(arena.available() >= maxSize / 3);

  TEST_ASSERT(arena.rawBuffer() != nullptr);
}

static void test_emptyArena() {
  auto arena = DynArena<char, 4>(0);

  TEST_ASSERT_MESSAGE(arena.capacity() == 0, "Capacity is non-zero");
  TEST_ASSERT_MESSAGE(arena.available() == 0, "Available size is non-zero");

  TEST_ASSERT_FALSE(arena.setAvailable(10));
  TEST_ASSERT_FALSE(arena.setAvailable(4));
  TEST_ASSERT(arena.setAvailable(0));
}

static void test_realloc() {
  const auto maxSize = 200;
  auto arena = DynArena<char, 4>(maxSize);

  // Ensure the allocation isn't clearly too aggressive.
  TEST_ASSERT(arena.setAvailable(maxSize / 10));
  auto availableAlloc1 = arena.available();
  TEST_ASSERT(availableAlloc1 < maxSize);

  // Set the available buffer.
  memset(arena.rawBuffer(), 'a', availableAlloc1);

  TEST_ASSERT(arena.setAvailable(maxSize));
  auto availableAlloc2 = arena.available();
  TEST_ASSERT(availableAlloc2 == maxSize);

  TEST_ASSERT(availableAlloc1 < availableAlloc2);

  // Check that the previous value is still there.
  const auto buff = arena.rawBuffer();
  for (auto i = 0; i < availableAlloc1; i++) {
    TEST_ASSERT(buff[i] == 'a');
  }
}


void runDynArenaTests() {
  RUN_TEST(test_simpleArena);
  RUN_TEST(test_emptyArena);
  RUN_TEST(test_realloc);
}
