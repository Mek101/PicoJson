#include "ArenaUtil.hh"

#include <unity.h>

#include <util/arena/StaticArena.hh>


template<typename T, uint16_t S> using StaticArena = PJ_IMPL_NAMESPACE::StaticArena<T, S>;

static void test_zeroSizedArena() {
  auto arena = StaticArena<char, 0>();

  TEST_ASSERT_MESSAGE(arena.available() == 0, "Space available not zero");
  TEST_ASSERT_MESSAGE(arena.capacity() == 0, "Capacity not zero");
  
  TEST_ASSERT(arena.setAvailable(0));
  TEST_ASSERT_FALSE(arena.setAvailable(10));
}

static void test_simpleArena() {
  auto arena = StaticArena<char, 10>();

  TEST_ASSERT(arena.capacity() == 10);

  TEST_ASSERT(arena.setAvailable(1));
  TEST_ASSERT(arena.available() >= 1);

  TEST_ASSERT(arena.setAvailable(10));
  TEST_ASSERT(arena.available() == 10);

  TEST_ASSERT_FALSE(arena.setAvailable(55));
  TEST_ASSERT(arena.available() == 10);

  TEST_ASSERT(arena.rawBuffer() != nullptr);
}


void runStaticArenaTests() {
  RUN_TEST(test_zeroSizedArena);
  RUN_TEST(test_simpleArena);
}
