#include "ArenaUtil.hh"

#include <unity.h>


void setUp() {
  // This can be empty. Unity requires these.
}

void tearDown() {
  // This can be empty. Unity requires these.
}

int main() {
  UNITY_BEGIN();
  runDynArenaTests();
  runStaticArenaTests();
  return UNITY_END();
}