#ifndef UNICODE_ARRAYS_H
#define UNICODE_ARRAYS_H


#define ARR_SIZE(a) (sizeof(a) / sizeof(a[0]))
#define BUFF_LEN 5

struct CodeunitRow {
  const char unicodeHex[BUFF_LEN];
  const char expected[BUFF_LEN];
};

struct CodepointPairRow {
  const char highSurrogate[BUFF_LEN];
  const char lowSurrogate[BUFF_LEN];
  const char expected[BUFF_LEN];
};

extern const CodeunitRow singleCodeunitRows[26];

extern const CodepointPairRow pairCodepointRows[25];

#endif
