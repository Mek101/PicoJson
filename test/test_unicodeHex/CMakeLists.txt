if (PJ_ESCAPED_UNICODE EQUAL ON)
    add_executable(UnicodeHexTest
        ${CMAKE_CURRENT_SOURCE_DIR}/UnicodeHex.cc
        ${CMAKE_CURRENT_SOURCE_DIR}/Convert.cc
        ${CMAKE_CURRENT_SOURCE_DIR}/Parse.cc
        ${CMAKE_CURRENT_SOURCE_DIR}/UnicodeArrays.cc
    )

    target_link_libraries(UnicodeHexTest
        ${CMAKE_PROJECT_NAME}
        unity
        TestHelpers
    )

    add_test(UnicodeHex UnicodeHexTest)
endif()
