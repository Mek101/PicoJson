#include <unity.h>

#include "Convert.hh"
#include "Parse.hh"


void setUp() {
  // This can be empty. Unity requires these.
}

void tearDown() {
  // This can be empty. Unity requires these.
}

int main() {
  UNITY_BEGIN();
  runUnicodeHexTest();
  runParseTest();
  return UNITY_END();
}
