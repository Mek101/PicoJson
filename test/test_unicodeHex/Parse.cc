#include "Parse.hh"

#include <stdint.h>
#include <string.h>

#include <unity.h>

#include <PicoJson.hh>

#include "UnicodeArrays.hh"
#include "Util.hh"


class ExpectStringHandle {
private:
  bool expectOnKey;
  const char* str;

  void testString(const char* other) {
    const auto equals = strcmp(this->str, other) == 0;

    if (equals) {
      this->appearances++;
      ;
    }
  }
public:
  uint16_t appearances;

  explicit ExpectStringHandle(bool expectOnKey, const char* str) : expectOnKey(expectOnKey), str(str), appearances(0) {}

  void onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth) {}
  void onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth) {}

  void onKey(const char* const key) {
    if (this->expectOnKey) {
      this->testString(key);
    }
  }

  void onValue(const pj::JsonValue& value) {
    if (!this->expectOnKey && value.getValueType() == pj::JsonValueType::String) {
      this->testString(value.getStringUnchecked());
    }
  }
};


template<typename Parser_t> static const char* put(Parser_t& parser, const char c) {
  pj::ParseError err = parser.parse(c);
  if (err != pj::ParseError::None) {
    return errorToString(err);
  }
  return nullptr;
}

static void escapeUnicode(const char* unicodeHex, char* dest) {
  dest[0] = '\\';
  dest[1] = 'u';
  memcpy(dest + 2, unicodeHex, 4);
}

template<typename Parser_t> static const char* feedTo(const bool inKey, const char* buff, Parser_t& parser) {
  const char* err = nullptr;

  err = put(parser, '{');
  if (err != nullptr) {
    return err;
  }

  err = put(parser, '"');
  if (err != nullptr) {
    return err;
  }

  for (auto s = inKey ? buff : "key"; *s != '\0'; s += 1) {
    err = put(parser, *s);
    if (err != nullptr) {
      return err;
    }
  }

  err = put(parser, '"');
  if (err != nullptr) {
    return err;
  }

  err = put(parser, ':');
  if (err != nullptr) {
    return err;
  }

  err = put(parser, '"');
  if (err != nullptr) {
    return err;
  }

  for (auto s = inKey ? "value" : buff; *s != '\0'; s += 1) {
    err = put(parser, *s);
    if (err != nullptr) {
      return err;
    }
  }

  err = put(parser, '"');
  if (err != nullptr) {
    return err;
  }

  err = put(parser, '}');
  if (err != nullptr) {
    return err;
  }

  return err;
}

static void testSingle(const bool inKey) {
  char buff[7];

  for (auto i = 0; i < ARR_SIZE(singleCodeunitRows); i++) {
    const CodeunitRow& r = singleCodeunitRows[i];
    auto p = pj::StaticParser<200, 5, ExpectStringHandle>(ExpectStringHandle(inKey, r.expected));
    escapeUnicode(r.unicodeHex, buff);

    auto feedErr = feedTo(inKey, buff, p);
    TEST_ASSERT_MESSAGE(feedErr == nullptr, "Error on parsing");
    TEST_ASSERT_EQUAL_MESSAGE(1, p.handle().appearances, "Expected string did not appear");
  }
}

static void testDouble(const bool inKey) {
  char buff[13];
  
  for (auto i = 0; i < ARR_SIZE(pairCodepointRows); i++) {
    const CodepointPairRow& r = pairCodepointRows[i];
    auto p = pj::StaticParser<200, 5, ExpectStringHandle>(ExpectStringHandle(true, r.expected));
    escapeUnicode(r.highSurrogate, buff);
    escapeUnicode(r.lowSurrogate, buff + 6);

    auto feedErr = feedTo(true, buff, p);
    TEST_ASSERT_MESSAGE(feedErr == nullptr, "Error on parsing");
    TEST_ASSERT_EQUAL_MESSAGE(1, p.handle().appearances, "Expected string did not appear");
  }
}

static void testMissingSurrogate(const bool inKey) {
  char buff[7];

  for (auto i = 0; i < ARR_SIZE(pairCodepointRows); i++) {
    const CodepointPairRow& r = pairCodepointRows[i];
    auto p = pj::StaticParser<200, 5, ExpectStringHandle>(ExpectStringHandle(inKey, r.expected));
    escapeUnicode(r.highSurrogate, buff);

    auto feedErr = feedTo(inKey, buff, p);
    TEST_ASSERT_MESSAGE(feedErr != nullptr, "No error on parsing");
    TEST_ASSERT_EQUAL_MESSAGE(0, p.handle().appearances, "Unexpected string appeared");
  }
}


static void test_singleCodeunitOnKey() {
  testSingle(true);
}

static void test_singleCodeunitOnValue() {
  testSingle(false);
}

static void test_pairCodepointsOnKey() {
  testDouble(true);
}

static void test_pairCodepointsOnValue() {
  testDouble(false);
}

static void test_missingSurrogateOnKey() {
  testMissingSurrogate(true);
}

static void test_missingSurrogateOnValue() {
  testMissingSurrogate(false);
}


void runParseTest() {
  RUN_TEST(test_singleCodeunitOnKey);
  RUN_TEST(test_singleCodeunitOnValue);
  RUN_TEST(test_pairCodepointsOnKey);
  RUN_TEST(test_pairCodepointsOnValue);
  RUN_TEST(test_missingSurrogateOnKey);
  RUN_TEST(test_missingSurrogateOnValue);
}
