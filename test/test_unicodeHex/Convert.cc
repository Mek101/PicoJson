#include <unity.h>

#include <stdint.h>
#include <string.h>

#include <util/Convert.hh>

#include "UnicodeArrays.hh"


static void test_convertSingleCodeunit() {
  char buffer[BUFF_LEN];

  for (auto i = 0; i < ARR_SIZE(singleCodeunitRows); i++) {
    const CodeunitRow& r = singleCodeunitRows[i];
    const auto expectedStrLen = strlen(r.expected);
    memcpy(buffer, r.unicodeHex, BUFF_LEN);

    const auto err = PJ_IMPL_NAMESPACE::tryConvertInplace(buffer, false);

    TEST_ASSERT_EQUAL(PJ_IMPL_NAMESPACE::UTF16ConversionError::None, err.err);
    TEST_ASSERT_EQUAL(expectedStrLen, err.written);
    TEST_ASSERT_EQUAL_CHAR_ARRAY(r.expected, buffer, expectedStrLen);
  }
}

static void test_convertCodepointPair() {
  char buffer[(BUFF_LEN * 2) - 1];

  for (auto i = 0; i < ARR_SIZE(pairCodepointRows); i++) {
    const CodepointPairRow& r = pairCodepointRows[i];
    const auto expectedStrLen = strlen(r.expected);
    memcpy(buffer, r.highSurrogate, BUFF_LEN - 1);
    memcpy(buffer + BUFF_LEN - 1, r.lowSurrogate, BUFF_LEN);

    const auto err = PJ_IMPL_NAMESPACE::tryConvertInplace(buffer, true);

    TEST_ASSERT_EQUAL(PJ_IMPL_NAMESPACE::UTF16ConversionError::None, err.err);
    TEST_ASSERT_EQUAL(expectedStrLen, err.written);
    TEST_ASSERT_EQUAL_CHAR_ARRAY(r.expected, buffer, expectedStrLen);
  }
}

static void test_detectIncompleteCodepointPair() {
  char buffer[BUFF_LEN];

  for (auto i = 0; i < ARR_SIZE(pairCodepointRows); i++) {
    const CodepointPairRow& r = pairCodepointRows[i];
    memcpy(buffer, r.highSurrogate, BUFF_LEN);

    const auto err = PJ_IMPL_NAMESPACE::tryConvertInplace(buffer, false);

    TEST_ASSERT_EQUAL(PJ_IMPL_NAMESPACE::UTF16ConversionError::IncompletePair, err.err);
    // The buffer must be left unchanged on failure.
    TEST_ASSERT_EQUAL_CHAR_ARRAY(r.highSurrogate, buffer, BUFF_LEN);
  }
}


void runUnicodeHexTest() {
  RUN_TEST(test_convertSingleCodeunit);
  RUN_TEST(test_convertCodepointPair);
  RUN_TEST(test_detectIncompleteCodepointPair);
}
