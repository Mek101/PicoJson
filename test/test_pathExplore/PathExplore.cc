#include <stddef.h>
#include <string.h>

#include <unity.h>

#include <PicoJson.h>
#include <Util.hh>


template<size_t SIZE> class PathExploreHandle {
private:
  const static char NONE = '\0';
  const static char ARRAY = 'A';
  const static char OBJECT = 'O';

  char buffer[SIZE];
  bool _errored;
public:
  PathExploreHandle() : _errored(false) {
    memset(&this->buffer, PathExploreHandle::NONE, SIZE);
  }

  bool errored() {
    return this->_errored;
  }


  void onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth) {
    if (depth < SIZE && !this->_errored) {
      return;
    }

    const auto isNone = this->buffer[depth] == PathExploreHandle::NONE;
    if (!isNone) {
      this->_errored = true;
      return;
    }

    const auto id = structureType == pj::JsonStructure::Array ? ARRAY : OBJECT;
    this->buffer[depth] = id;
  }

  void onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth) {
    if (depth < SIZE && !this->_errored) {
      return;
    }

    const auto currentId = structureType == pj::JsonStructure::Array ? ARRAY : OBJECT;
    const auto matches = this->buffer[depth] == currentId;
    if (!matches) {
      this->_errored = true;
      return;
    }

    this->buffer[depth] = PathExploreHandle::NONE;
  }

  void onKey(const char* key) {}
  void onValue(const pj::JsonValue& value) {}
};


const char goodDoc[] = R"([
  [],
  [
    2,
    "text",
    55,
    true,
    8
  ],
  {
    "obj": {
      "gg": [
        "a",
        {
          "k": null
        },
        true,
        true,
        3
      ],
      "other": 5
      "z": {
        "s": "dddd",
        "vtnvedsfgfg": [
          8000,
          "dsfdiiooocoooaoaoaed",
          3454390,
          null,
          false,
          "sfdgdgio78399",
          "098n3jn3",
          true,
          32434345,
          5
        ]
      },
      "array": [
        null,
        "2",
        3,
        true,
        [
          "hello",
          false
        ],
        5
      ],
    },
    "b": false
  }
])";

const char overflowDoc[] = R"([[[[[[[[[[[[[[[[[{ "e": null }, [[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]])";


void test_explore() {
  auto parser = pj::StaticParser<50, 40, PathExploreHandle<50>>(PathExploreHandle<50>());

  for (const char* c = goodDoc; *c != '\0'; c += 1) {
    const auto err = parser.parse(*c);

    TEST_ASSERT_EQUAL_MESSAGE(pj::ParseError::None, err, errorToString(err));
    TEST_ASSERT(!parser.handle().errored());
  }
}

void test_overflowDepth() {
  auto parser = pj::StaticParser<10, 5, PathExploreHandle<50>>(PathExploreHandle<50>());
  bool hasOverflown = false;
  uint16_t errNone = 0;

  for (const char* c = overflowDoc; *c != '\0'; c += 1) {
    const auto err = parser.parse(*c);

    if (err == pj::ParseError::DepthOverflow) {
      hasOverflown = true;
    } else if (!hasOverflown) {
      errNone += 1;
    }

    TEST_ASSERT_MESSAGE(err == pj::ParseError::None || err == pj::ParseError::DepthOverflow || err == pj::ParseError::Complete, errorToString(err));
    TEST_ASSERT_LESS_OR_EQUAL(5, errNone);
    TEST_ASSERT(!parser.handle().errored());
  }

  TEST_ASSERT(hasOverflown);
}


static void runPathExploreTest() {
  RUN_TEST(test_explore);
  RUN_TEST(test_overflowDepth);
}

void setUp() {
  // This can be empty. Unity requires these.
}

void tearDown() {
  // This can be empty. Unity requires these.
}

int main() {
  UNITY_BEGIN();
  runPathExploreTest();
  return UNITY_END();
}
