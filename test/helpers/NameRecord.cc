#include "NameRecord.hh"

void NameRecord::assertType() const {
  if (this->type == Type::Untyped) {
    TEST_MESSAGE("Use of untyped name record!");
  } else {
    const auto isAny = type == Type::Index || type == Type::Key || type == Type::AnonymousIndex || type == Type::AnonymousKey;
    TEST_ASSERT_MESSAGE(isAny, "Invalid name type");
  }
}

std::string NameRecord::toString() {
  std::string str = "Name record: ";

  switch (this->type) {
    case Type::Index:
      str.append("Array index: [");
      str.append(std::to_string(this->index));
      str.push_back(']');
      break;
    case Type::Key:
      str.append("Object key: {");
      str.append(this->key);
      str.push_back('}');
    case Type::AnonymousIndex:
      str.append("Array anonymous [?]");
      break;
    case Type::AnonymousKey:
      str.append("Object anonymous {?}");
      break;
    default:
      str.append("Error invalid type");
      break;
  }

  return str;
}