#include "EventRecordHandle.hh"


static ValueRecord jsonValueToRecord(const pj::JsonValue& value) {
  switch (value.getValueType()) {
    case pj::JsonValueType::String:
      return ValueRecord::stringValue(value.getStringUnchecked());
    case pj::JsonValueType::Float:
      return ValueRecord::floatValue(value.getFloatUnchecked());
    case pj::JsonValueType::Integer:
      return ValueRecord::integerValue(value.getIntegerUnchecked());
    case pj::JsonValueType::Bool:
      return ValueRecord::boolValue(value.getBoolUnchecked());
    default:
    case pj::JsonValueType::Null:
      return ValueRecord::nullValue();
  }
}


EventRecordHandle::EventRecordHandle() : errored(false), currentStructureType(PJ_IMPL_NAMESPACE::JsonStructureType::Invalid) {}

bool EventRecordHandle::isErrored() {
  return this->errored;
}

std::vector<Record>* EventRecordHandle::getEvents() {
  return &this->buff;
}

ssize_t EventRecordHandle::getLastStructureBegin() {
  for (auto i = 0; i < this->buff.size(); i++) {
    if (this->buff[i].value.isStructureBegin()) {
      return i;
    }
  }
  return -1;
}


void EventRecordHandle::onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth) {
  // Document root?
  if (this->buff.empty() && depth == 0) {
    NameRecord name;
    if (structureType == pj::JsonStructure::Array) {
      name = NameRecord::arrayIndexAnonymous();
    } else {
      name = NameRecord::objectKeyAnonymous();
    }

    this->buff.push_back(Record(name, ValueRecord::structureBeginValue(structureType)));
  } else if (!this->buff.empty() && depth != 0) {
    if ((this->currentStructureType == PJ_IMPL_NAMESPACE::JsonStructureType::Array && structureType != pj::JsonStructure::Array)
        || (this->currentStructureType == PJ_IMPL_NAMESPACE::JsonStructureType::Object && structureType != pj::JsonStructure::Object)) {
      this->errored = true;
      return;
    }


    NameRecord name;
    if (this->currentStructureType == PJ_IMPL_NAMESPACE::JsonStructureType::Array) {
      this->lastIndex++;
      name = NameRecord::arrayIndexName(this->lastIndex);
    } else {
      // Must have a key if it's inside an object.
      if (!this->lastKeyAny) {
        this->errored = true;
        return;
      }

      name = NameRecord::objectKeyName(this->lastKey);
      this->lastKeyAny = false;
      this->lastKey = std::string();
    }

    this->buff.push_back(Record(name, ValueRecord::structureBeginValue(structureType)));
  } else {
    this->errored = true;
    return;
  }

  if (structureType == pj::JsonStructure::Array) {
    this->lastIndex = -1;
    this->currentStructureType = PJ_IMPL_NAMESPACE::JsonStructureType::Array;
  } else {
    this->lastKeyAny = false;
    this->lastKey = std::string();
    this->currentStructureType = PJ_IMPL_NAMESPACE::JsonStructureType::Object;
  }
}

void EventRecordHandle::onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth) {
  auto i = this->getLastStructureBegin();
  if (i < 0) {
    this->errored = true;
    return;
  }

  auto currentName = &this->buff[i].name;

  NameRecord name;
  if (currentName->isArrayIndex()) {
    this->lastIndex = *currentName->getIndex();

    if (currentName->hasName()) {
      name = NameRecord::arrayIndexName(this->lastIndex);
    } else {
      name = NameRecord::arrayIndexAnonymous();
    }

    this->currentStructureType = PJ_IMPL_NAMESPACE::JsonStructureType::Array;
  } else if (this->buff[i].name.isObjectKey()) {
    // Cannot have a key at the end of an object.
    if (this->lastKeyAny) {
      this->errored = true;
      return;
    }

    if (currentName->hasName()) {
      auto key = *currentName->getKey();
      this->lastKeyAny = true;
      this->lastKey = std::string(key);
      name = NameRecord::objectKeyName(key);
    } else {
      name = NameRecord::objectKeyAnonymous();
    }

    this->currentStructureType = PJ_IMPL_NAMESPACE::JsonStructureType::Object;
  }

  auto value = ValueRecord::structureEndValue(*this->buff[i].value.getStructureBegin());
  this->buff.push_back(Record(name, value));
}

void EventRecordHandle::onKey(const char* const key) {
  // There should have been a value consuming the key. No keys in arrays.
  if (this->lastKeyAny || this->currentStructureType == PJ_IMPL_NAMESPACE::JsonStructureType::Array) {
    this->errored = true;
    return;
  }

  this->lastKeyAny = true;
  this->lastKey = std::string(key);
}

void EventRecordHandle::onValue(const pj::JsonValue& value) {
  NameRecord name;
  if (this->currentStructureType == PJ_IMPL_NAMESPACE::JsonStructureType::Array) {
    this->lastIndex++;

    name = NameRecord::arrayIndexName(this->lastIndex);
  } else if (this->currentStructureType == PJ_IMPL_NAMESPACE::JsonStructureType::Object) {
    // Values must be preceeded by a key inside an object.
    if (!this->lastKeyAny) {
      this->errored = true;
      return;
    }

    name = NameRecord::objectKeyName(this->lastKey);
    this->lastKeyAny = true;
    this->lastKey = std::string();
  } else {
    this->errored = true;
    return;
  }

  ValueRecord valueR = jsonValueToRecord(value);

  this->buff.push_back(Record(name, valueR));
}
