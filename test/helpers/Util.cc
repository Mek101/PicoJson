#include "Util.hh"

#include <unity.h>


const char *const strings[] = {
  "None",
  "StringOverflow",
  "UnescapedControlCharacter",
  "BadUnicodeCodepoint",
  "BadDocument",
  "BadArray",
  "BadObject",
  "BadKey",
  "BadValue",
  "BadNumber",
  "BadLiteral",
  "BadEscapeCharacter",
  "BadUnicode",
  "Complete",
  "InvalidState",
  "BufferOverflow",
  "DepthOverflow",
};


const char *const errorToString(const pj::ParseError error) {
  return strings[static_cast<uint8_t>(error)];
}

void parseDocumentWith(pj::SlabParser<VirtualHandleProxy>& parser, const char* const doc) {
  for (auto i = 0; doc[i] != '\0'; i++) {
    auto c = doc[i];
    auto err = parser.parse(c);

    TEST_ASSERT_EQUAL_MESSAGE(pj::ParseError::None, err, errorToString(err));
  }
}


void assertNameIndex(const NameRecord& record, const uint32_t expected) {
  TEST_ASSERT(record.isArrayIndex());

  auto index = *record.getIndex();
  auto indexStr = std::to_string(index);
  TEST_ASSERT_EQUAL_MESSAGE(expected, index, indexStr.c_str());
}

void assertNameKey(const NameRecord& record, const std::string expected) {
  TEST_ASSERT(record.isObjectKey());

  auto key = *record.getKey();
  TEST_ASSERT_MESSAGE(expected == key, key.c_str());
}


void assertValueString(const ValueRecord& record, const std::string expected) {
  TEST_ASSERT(record.isString());

  auto stringV = *record.getString();
  TEST_ASSERT_MESSAGE(expected == stringV, stringV.c_str());
}

void assertValueInteger(const ValueRecord& record, const int32_t expected) {
  const auto str = record.toString();
  TEST_ASSERT_MESSAGE(record.isInteger(), str.c_str());

  auto integerV = *record.getInteger();
  auto integerStr = std::to_string(integerV);
  TEST_ASSERT_EQUAL_MESSAGE(expected, integerV, integerStr.c_str());
}

void assertValueFloat(const ValueRecord& record, const float expected) {
  const auto str = record.toString();
  TEST_ASSERT_MESSAGE(record.isFloat(), str.c_str());

  auto floatV = *record.getFloat();
  auto floatStr = std::to_string(floatV);
  TEST_ASSERT_EQUAL_FLOAT_MESSAGE(expected, floatV, floatStr.c_str());
}

void assertValueBool(const ValueRecord& record, const bool expected) {
  TEST_ASSERT(record.isBool());

  auto boolV = *record.getBool();
  auto boolStr = std::to_string(boolV);
  TEST_ASSERT_EQUAL_MESSAGE(expected, boolV, boolStr.c_str());
}

void assertValueNull(const ValueRecord& record) {
  TEST_ASSERT(record.isNull());
}
