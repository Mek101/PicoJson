#include "ValueRecord.hh"

#include <unity.h>


void ValueRecord::assertType() const {
  const auto isAny = type == Type::String || type == Type::Integer || type == Type::Float || type == Type::Bool
                     || type == Type::Null || type == Type::StructureBegin || type == Type::StructureEnd;
  TEST_ASSERT_MESSAGE(isAny, "Invalid value type");
}

std::string ValueRecord::toString() const {
  std::string str = "Value record: ";

  switch (this->type) {
    case Type::String:
      str.push_back('"');
      str.append(this->stringV);
      str.push_back('"');
      break;
    case Type::Integer:
      str.append(std::to_string(this->integerV));
      break;
    case Type::Float:
      str.append(std::to_string(this->floatV));
      break;
    case Type::Bool:
      str.append(std::to_string(this->boolV));
      break;
    case Type::Null:
      str.append("null");
      break;
    case Type::StructureBegin:
      if (this->structureV == pj::JsonStructure::Array) {
        str.append("Array begin");
      } else {
        str.append("Object begin");
      }
      break;
    case Type::StructureEnd:
      if (this->structureV == pj::JsonStructure::Array) {
        str.append("Array end");
      } else {
        str.append("Object end");
      }
      break;
    default:
      str.append("Error invalid type");
      break;
  }

  return str;
}