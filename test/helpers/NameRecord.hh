#ifndef NAME_RECORD_H
#define NAME_RECORD_H

#include <string>

#include <stdint.h>

#include <unity.h>


class NameRecord {
private:
  enum class Type {
    Untyped,
    Index,
    Key,
    AnonymousIndex,
    AnonymousKey
  };


  int32_t index;
  std::string key;

  Type type;


  void assertType() const;
public:
  inline NameRecord() : type(Type::Untyped) {}

  static inline NameRecord arrayIndexName(const int32_t index) {
    auto record = NameRecord();
    record.index = index;
    record.type = Type::Index;
    return record;
  }

  static inline NameRecord objectKeyName(const std::string key) {
    auto record = NameRecord();
    record.key = key;
    record.type = Type::Key;
    return record;
  }

  static inline NameRecord arrayIndexAnonymous() {
    auto record = NameRecord();
    record.type = Type::AnonymousIndex;
    return record;
  }

  static inline NameRecord objectKeyAnonymous() {
    auto record = NameRecord();
    record.type = Type::AnonymousKey;
    return record;
  }

  inline bool isArrayIndex() const {
    this->assertType();

    return this->type == Type::Index;
  }

  inline bool isObjectKey() const {
    this->assertType();

    return this->type == Type::Key;
  }

  inline bool hasName() const {
    this->assertType();

    return this->type == Type::Index || this->type == Type::Key;
  }

  inline const int32_t *const getIndex() const {
    this->assertType();

    return this->type == Type::Index ? &index : nullptr;
  }

  inline const std::string *const getKey() const {
    this->assertType();

    return this->type == Type::Key ? &key : nullptr;
  }

  std::string toString();
};

#endif