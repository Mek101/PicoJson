#include "DummyHandle.hh"


void DummyHandle::onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth) {}

void DummyHandle::onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth) {}

void DummyHandle::onKey(const char* const key) {}

void DummyHandle::onValue(const pj::JsonValue& value) {}
