#ifndef VALUE_RECORD_H
#define VALUE_RECORD_H

#include <string>

#include <PicoJson.hh>

#include <unity.h>


class ValueRecord {
private:
  enum class Type {
    String,
    Integer,
    Float,
    Bool,
    Null,
    StructureBegin,
    StructureEnd,
  };

  std::string stringV;
  int32_t integerV;
  float floatV;
  bool boolV;
  pj::JsonStructure structureV;

  Type type;


  void assertType() const;
public:
  static ValueRecord stringValue(const std::string stringValue) {
    auto record = ValueRecord();
    record.type = Type::String;
    record.stringV = stringValue;
    return record;
  }

  static ValueRecord integerValue(const int32_t integerValue) {
    auto record = ValueRecord();
    record.type = Type::Integer;
    record.integerV = integerValue;
    return record;
  }

  static ValueRecord floatValue(const float floatValue) {
    auto record = ValueRecord();
    record.type = Type::Float;
    record.floatV = floatValue;
    return record;
  }

  static ValueRecord boolValue(const bool boolValue) {
    auto record = ValueRecord();
    record.type = Type::Bool;
    record.boolV = boolValue;
    return record;
  }

  static ValueRecord nullValue() {
    auto record = ValueRecord();
    record.type = Type::Null;
    return record;
  }

  static ValueRecord structureBeginValue(const pj::JsonStructure structureValue) {
    auto record = ValueRecord();
    record.type = Type::StructureBegin;
    record.structureV = structureValue;
    return record;
  }

  static ValueRecord structureEndValue(const pj::JsonStructure structureValue) {
    auto record = ValueRecord();
    record.type = Type::StructureEnd;
    record.structureV = structureValue;
    return record;
  }


  inline bool isString() const {
    this->assertType();

    return this->type == Type::String;
  }

  inline bool isInteger() const {
    this->assertType();

    return this->type == Type::Integer;
  }

  inline bool isFloat() const {
    this->assertType();

    return this->type == Type::Float;
  }

  inline bool isBool() const {
    this->assertType();

    return this->type == Type::Bool;
  }

  inline bool isNull() const {
    this->assertType();

    return this->type == Type::Null;
  }

  inline bool isStructureBegin() const {
    this->assertType();

    return this->type == Type::StructureBegin;
  }

  inline bool isStructureEnd() const {
    this->assertType();

    return this->type == Type::StructureEnd;
  }

  inline const std::string *const getString() const {
    this->assertType();

    return this->isString() ? &this->stringV : nullptr;
  }

  inline const int32_t *const getInteger() const {
    this->assertType();

    return this->isInteger() ? &this->integerV : nullptr;
  }

  inline const float *const getFloat() const {
    this->assertType();

    return this->isFloat() ? &this->floatV : nullptr;
  }

  inline const bool *const getBool() const {
    this->assertType();

    return this->isBool() ? &this->boolV : nullptr;
  }

  inline const pj::JsonStructure *const getStructureBegin() const {
    this->assertType();

    return this->isStructureBegin() ? &this->structureV : nullptr;
  }

  inline const pj::JsonStructure *const getStructureEnd() const {
    this->assertType();

    return this->isStructureEnd() ? &this->structureV : nullptr;
  }

  std::string toString() const;
};

#endif