#ifndef UTIL_H
#define UTIL_H

#include <string>

#include <stdint.h>

#include <PicoJson.hh>

#include "NameRecord.hh"
#include "ValueRecord.hh"
#include "EventRecordHandle.hh"
#include "VirtualHandleProxy.hh"


const char *const errorToString(const pj::ParseError error);


void parseDocumentWith(pj::SlabParser<VirtualHandleProxy>& parser, const char* const doc);


void assertNameIndex(const NameRecord& record, const uint32_t expected);

void assertNameKey(const NameRecord& record, const std::string expected);


void assertValueString(const ValueRecord& record, const std::string expected);

void assertValueInteger(const ValueRecord& record, const int32_t expected);

void assertValueFloat(const ValueRecord& record, const float expected);

void assertValueBool(const ValueRecord& record, const bool expected);

void assertValueNull(const ValueRecord& record);

#endif
