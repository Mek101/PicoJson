#ifndef VIRTUAL_HANDLE_H
#define VIRTUAL_HANDLE_H

#include <stdint.h>

#include <PicoJson.hh>


class VirtualHandle {
public:
  virtual ~VirtualHandle() = default;

  virtual void onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth) = 0;
  virtual void onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth) = 0;

  virtual void onKey(const char* const key) = 0;

  virtual void onValue(const pj::JsonValue& value) = 0;
};

#endif
