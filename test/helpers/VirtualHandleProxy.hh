#ifndef VIRTUAL_HANDLE_PROXY_H
#define VIRTUAL_HANDLE_PROXY_H

#include <PicoJson.hh>

#include "VirtualHandle.hh"


/**
 * A handle that serves as a proxy to a dynamicly settable handle.
 */
class VirtualHandleProxy {
private:
  VirtualHandle* handle;
public:
  inline VirtualHandleProxy() : handle(nullptr) {}
  inline explicit VirtualHandleProxy(VirtualHandle* const handle) : handle(handle) {}

  VirtualHandleProxy(const VirtualHandleProxy& other) noexcept = default;
  VirtualHandleProxy& operator=(const VirtualHandleProxy& other) noexcept = default;

  VirtualHandleProxy(VirtualHandleProxy&& other) noexcept = default;
  VirtualHandleProxy& operator=(VirtualHandleProxy&& other) noexcept = default;


  inline VirtualHandle* resetHandle(VirtualHandle* const handle) noexcept {
    auto old = this->handle;
    this->handle = handle;
    return old;
  }

  inline VirtualHandle* getHandle() noexcept {
    return this->handle;
  }

  inline void onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth) {
    this->handle->onStructureBegin(structureType, depth);
  }

  inline void onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth) {
    this->handle->onStructureEnd(structureType, depth);
  }

  inline void onKey(const char* const key) {
    this->handle->onKey(key);
  }

  inline void onValue(const pj::JsonValue& value) {
    this->handle->onValue(value);
  }
};

#endif
