#ifndef EVENT_RECORD_HANDLE_H
#define EVENT_RECORD_HANDLE_H

#include <vector>
#include <string>
#include <optional>

#include <stdint.h>

#include <PicoJson.hh>

#include "NameRecord.hh"
#include "ValueRecord.hh"
#include "VirtualHandle.hh"


struct Record {
  NameRecord name;
  ValueRecord value;

  Record() = default;
  inline explicit Record(const NameRecord name, const ValueRecord value) : name(name), value(value) {}
};


class EventRecordHandle : public VirtualHandle {
private:
  std::vector<Record> buff;

  bool errored;

  PJ_IMPL_NAMESPACE::JsonStructureType currentStructureType;
  uint64_t lastIndex;
  bool lastKeyAny;
  std::string lastKey;

  ssize_t getLastStructureBegin();
public:
  EventRecordHandle();
  virtual ~EventRecordHandle() override = default;

  bool isErrored();
  std::vector<Record>* getEvents();

  virtual void onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth) override;
  virtual void onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth) override;
  virtual void onKey(const char* const key) override;
  virtual void onValue(const pj::JsonValue& value) override;
};

#endif