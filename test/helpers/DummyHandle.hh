#ifndef DUMMY_HANDLE_H
#define DUMMY_HANDLE_H

#include <PicoJson.hh>

#include "VirtualHandle.hh"


class DummyHandle : public VirtualHandle
{
public:
  virtual ~DummyHandle() override = default;
  virtual void onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth) override;
  virtual void onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth) override;
  virtual void onKey(const char* const key) override;
  virtual void onValue(const pj::JsonValue& value) override;
};

#endif