#include <unity.h>

#include <float.h>

#include <PicoJson.h>

#include <Util.hh>


class OutOfRangeHandle {
public:
  enum class Error {
    None,
    BadKey,
    BadValue,
    StringNotTruncated,
    IntegerNotOverflow,
    FloatNotOutOfRange,
  };

  Error err;
  bool outOfRangeDetected;


  OutOfRangeHandle() : err(Error::None), outOfRangeDetected(false) {}

  void onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth) {}

  void onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth) {}

  void onKey(const char* key) {
    if (this->err == Error::None) {
      this->err = (strcmp(key, "num") == 0 || strcmp(key, "str") == 0) ? Error::None : Error::BadKey;
    }
  }

  void onValue(const pj::JsonValue& value) {
    if (this->err == Error::None) {
      switch (value.getValueType()) {
        case pj::JsonValueType::String:
          this->outOfRangeDetected = value.isStringTruncated();
          this->err = value.isStringTruncated() ? Error::None : Error::StringNotTruncated;
          break;
        case pj::JsonValueType::Integer:
          this->outOfRangeDetected = value.isIntegerOverflow();
          this->err = value.isIntegerOverflow() ? Error::None : Error::IntegerNotOverflow;
          break;
        case pj::JsonValueType::Float:
          if (value.isFloatOverflow()) {
            this->outOfRangeDetected = true;
            this->err = Error::None;
          } else if (value.isFloatUnderflow()) {
            this->outOfRangeDetected = true;
            this->err = Error::None;
          } else {
            this->err = Error::FloatNotOutOfRange;
          }
          break;
        default:
          this->err = Error::BadValue;
          break;
      }
    }
  }
};


const char stringDoc[] = R"({
  "str":
  "long ssttrrrrriiiiiiiiinnnnnnnnnnnnnnnggggggggggggggggggggggggggggggggggg"
})";

const char integerDoc[] = R"({
  "num": 99999999999999999999999999999999999999999999999999999999
})";

#if PJ_FLOATING_POINT_NUM_PARSE == 1
const char floatOverflowDoc[] = R"({
  "num": 999888888888833333333333333334444444444444444444444445666.7777777789999999999999999999999765556787
})";

const char floatUnderflowDoc[] = R"({
  "num": 2.22507385850720138309023271733240406e-308
})";
#endif


void test_stringOverflow() {
  auto parser = pj::StaticParser<20, 5, OutOfRangeHandle>(OutOfRangeHandle());

  bool overflowDetected = false;

  for (auto i = 0; i < sizeof(stringDoc); i++) {
    const auto err = parser.parse(stringDoc[i]);

    if (err == pj::ParseError::StringOverflow) {
      overflowDetected = true;
    }

    TEST_ASSERT_MESSAGE(err == pj::ParseError::None || err == pj::ParseError::StringOverflow, errorToString(err));
    TEST_ASSERT(parser.handle().err == OutOfRangeHandle::Error::None);
  }

  TEST_ASSERT(overflowDetected);
  TEST_ASSERT(parser.handle().outOfRangeDetected);
}

void test_integerOverflow() {
  auto parser = pj::StaticParser<200, 5, OutOfRangeHandle>(OutOfRangeHandle());

  for (auto i = 0; i < sizeof(integerDoc); i++) {
    const auto err = parser.parse(integerDoc[i]);

    TEST_ASSERT_EQUAL_MESSAGE(pj::ParseError::None, err, errorToString(err));
    TEST_ASSERT(parser.handle().err == OutOfRangeHandle::Error::None);
  }

  TEST_ASSERT(parser.handle().outOfRangeDetected);
}

#if PJ_FLOATING_POINT_NUM_PARSE == 1
void test_floatOverflow() {
  auto parser = pj::StaticParser<200, 5, OutOfRangeHandle>(OutOfRangeHandle());

  for (auto i = 0; i < sizeof(floatOverflowDoc); i++) {
    const auto err = parser.parse(floatOverflowDoc[i]);

    TEST_ASSERT_EQUAL_MESSAGE(pj::ParseError::None, err, errorToString(err));
    TEST_ASSERT(parser.handle().err == OutOfRangeHandle::Error::None);
  }

  TEST_ASSERT(parser.handle().outOfRangeDetected);
}

void test_floatUnderflow() {
  auto parser = pj::StaticParser<200, 5, OutOfRangeHandle>(OutOfRangeHandle());

  for (auto i = 0; i < sizeof(floatUnderflowDoc); i++) {
    const auto err = parser.parse(floatUnderflowDoc[i]);

    TEST_ASSERT_EQUAL_MESSAGE(pj::ParseError::None, err, errorToString(err));
    TEST_ASSERT(parser.handle().err == OutOfRangeHandle::Error::None);
  }

  TEST_ASSERT(parser.handle().outOfRangeDetected);
}
#endif

void runOutOfRangeTest() {
  RUN_TEST(test_stringOverflow);
  RUN_TEST(test_integerOverflow);

#if PJ_FLOATING_POINT_NUM_PARSE == 1
  RUN_TEST(test_floatOverflow);
  RUN_TEST(test_floatUnderflow);
#endif

}

void setUp() {
  // This can be empty. Unity requires these.
}

void tearDown() {
  // This can be empty. Unity requires these.
}

int main() {
  UNITY_BEGIN();
  runOutOfRangeTest();
  return UNITY_END();
}
