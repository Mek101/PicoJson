#include "ImplComponents.hh"

#include <unity.h>


void setUp() {
  // This can be empty. Unity requires these.
}

void tearDown() {
  // This can be empty. Unity requires these.
}

int main() {
  UNITY_BEGIN();
  runCharBufferTests();
  runStackPathTests();
  return UNITY_END();
}