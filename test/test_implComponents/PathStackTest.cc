#include "ImplComponents.hh"

#include <stdint.h>
#include <stdlib.h>

#include <unity.h>

#include <parse/impl/PathStack.hh>

#include <util/arena/SlabArena.hh>
#include <util/BytesBits.hh>


template<typename T> using SlabArena = PJ_IMPL_NAMESPACE::SlabArena<T>;
template<template<typename> typename A> using PathStack = PJ_IMPL_NAMESPACE::PathStack<A>;
using JsonStructureType = PJ_IMPL_NAMESPACE::JsonStructureType;

static PathStack<SlabArena> getStackPath(const uint16_t size, const uint16_t bits) {
  auto buff = reinterpret_cast<uint8_t*>(malloc(size * sizeof(char)));
  return PathStack<SlabArena>(SlabArena<uint8_t>(buff, size), bits);
}


static void test_stackPathEmpty() {
  auto stackPath = getStackPath(PJ_IMPL_NAMESPACE::bytesForBitset(0), 0);

  TEST_ASSERT(stackPath.size() == 0);
  TEST_ASSERT(stackPath.peekHead() == JsonStructureType::Invalid);
  TEST_ASSERT(stackPath.pop() == JsonStructureType::Invalid);
  TEST_ASSERT_FALSE(stackPath.push(JsonStructureType::Array));
}

static void test_pushPop() {
  const auto numBits = 12;
  auto stackPath = getStackPath(PJ_IMPL_NAMESPACE::bytesForBitset(numBits), numBits);

  TEST_ASSERT_MESSAGE(stackPath.size() == 0, "New PathStack is not empty");

  for (auto i = 0; i < numBits; i++) {
    auto structure = i % 2 == 0 ? JsonStructureType::Array : JsonStructureType::Object;

    TEST_ASSERT_MESSAGE(stackPath.push(structure), "Push error");
  }

  TEST_ASSERT_FALSE_MESSAGE(stackPath.size() == 0, "PathStack is not full");

  for (int i = numBits - 1; i >= 0; i--) {
    auto expectedStructure = i % 2 == 0 ? JsonStructureType::Array : JsonStructureType::Object;

    TEST_ASSERT_MESSAGE(stackPath.pop() == expectedStructure, "Unexpected structure");
  }

  TEST_ASSERT_MESSAGE(stackPath.size() == 0, "PathStack should be empty");
}

static void test_pushPeek() {
  const auto numBits = 12;
  auto stackPath = getStackPath(PJ_IMPL_NAMESPACE::bytesForBitset(numBits), numBits);

  TEST_ASSERT_MESSAGE(stackPath.size() == 0, "New PathStack is not empty");

  for (auto i = 0; i < numBits; i++) {
    auto structure = i % 2 == 0 ? JsonStructureType::Array : JsonStructureType::Object;

    TEST_ASSERT_MESSAGE(stackPath.push(structure), "Push error");
  }

  TEST_ASSERT_FALSE_MESSAGE(stackPath.size() == 0, "PathStack is not full");

  for (int i = numBits - 1; i >= 0; i--) {
    auto expectedStructure = i % 2 == 0 ? JsonStructureType::Array : JsonStructureType::Object;

    TEST_ASSERT_MESSAGE(stackPath.peekHead() == expectedStructure, "Unexpected structure");
    stackPath.popDestroyUnchecked();
    TEST_ASSERT_MESSAGE(i == 0 || !stackPath.size() == 0, "PathStack should still have elements");
  }

  TEST_ASSERT_MESSAGE(stackPath.size() == 0, "PathStack should be empty");
}

static void test_reset() {
  const auto numBits = 12;
  auto stackPath = getStackPath(PJ_IMPL_NAMESPACE::bytesForBitset(numBits), numBits);

  // Fill the stack path.
  for (auto i = 0; ; i++) {
    auto structure = i % 2 != 0 ? JsonStructureType::Array : JsonStructureType::Object;
    if (!stackPath.push(structure)) {
      break;
    }
  }

  TEST_ASSERT_FALSE(stackPath.size() == 0);

  stackPath.reset();

  // Ensure the reset was correct.
  TEST_ASSERT(stackPath.size() == 0);
  TEST_ASSERT(stackPath.peekHead() == JsonStructureType::Invalid);
  TEST_ASSERT(stackPath.pop() == JsonStructureType::Invalid);
  
  // Fill the stack path again
  for (auto i = 0; i < numBits; i++) {
    auto structure = i % 2 != 0 ? JsonStructureType::Array : JsonStructureType::Object;
    
    TEST_ASSERT(stackPath.push(structure));
  }

  TEST_ASSERT_FALSE(stackPath.push(JsonStructureType::Array));
}


void runStackPathTests() {
  RUN_TEST(test_stackPathEmpty);
  RUN_TEST(test_pushPop);
  RUN_TEST(test_pushPeek);
  RUN_TEST(test_reset);
}