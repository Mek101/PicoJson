#include "ImplComponents.hh"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <unity.h>

#include <parse/impl/CharBuffer.hh>

#include <util/arena/SlabArena.hh>


template<typename T> using SlabArena = PJ_IMPL_NAMESPACE::SlabArena<T>;
template<template<typename> typename A> using CharBuffer = PJ_IMPL_NAMESPACE::CharBuffer<A>;

static CharBuffer<SlabArena> getCharBuffer(const uint16_t size) {
  auto buff = reinterpret_cast<char*>(malloc(size * sizeof(char)));
  return CharBuffer<SlabArena>(SlabArena<char>(buff, size));
}


static void test_simpleTest() {
  auto size = 4;
  auto charBuffer = getCharBuffer(size);

  for (auto i = 0; i < size; i++) {
    TEST_ASSERT_MESSAGE(charBuffer.pushChar('a'), "Failed to push");
  }

  TEST_ASSERT_MESSAGE(charBuffer.bufferLength() == size, "Bad buffer size");


  TEST_ASSERT_FALSE(charBuffer.pushChar('a'));

  TEST_ASSERT_MESSAGE(charBuffer.bufferLength() == size, "Bad buffer size");
}

static void test_emptyCharBuffer() {
  auto charBuffer = getCharBuffer(0);

  TEST_ASSERT_FALSE(charBuffer.pushChar('a'));
  TEST_ASSERT_FALSE(charBuffer.pushOrReplaceChar('a'));
  TEST_ASSERT_FALSE(charBuffer.popDestroyBy(8));

  TEST_ASSERT(charBuffer.bufferLength() == 0);
  TEST_ASSERT_FALSE(charBuffer.ensureAvailable(8));
}

static void test_pushString() {
  auto charBuffer = getCharBuffer(sizeof("abc"));

  TEST_ASSERT(charBuffer.pushChar('a'));
  TEST_ASSERT(charBuffer.pushChar('b'));
  TEST_ASSERT(charBuffer.pushChar('c'));
  TEST_ASSERT(charBuffer.pushChar('\0'));

  auto str = charBuffer.rawBuffer();

  TEST_ASSERT(strcmp(str, "abc") == 0);
  TEST_ASSERT(charBuffer.bufferLength() == sizeof("abc"));
}

static void test_available() {
  auto size = 7;
  auto charBuffer = getCharBuffer(size);

  for (auto i = 0; i < size; i++) {
    TEST_ASSERT(charBuffer.ensureAvailable(size - i));
    TEST_ASSERT(charBuffer.pushChar('a'));
  }
}

static void test_overflowPushChar() {
  char supportBuffer[32];
  memcpy(supportBuffer, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", sizeof(supportBuffer));

  auto arenaSize = sizeof(supportBuffer) / 2;

  auto charBuffer = CharBuffer<SlabArena>(SlabArena<char>(supportBuffer, arenaSize));

  for (auto i = 0; i < arenaSize - 1; i++) {
    TEST_ASSERT_MESSAGE(charBuffer.pushChar('b'), "Push error");
  }
  TEST_ASSERT_MESSAGE(charBuffer.pushChar('\0'), "Push error");

  // Ensure that what should have been written is written.
  TEST_ASSERT_MESSAGE(memcmp(charBuffer.rawBuffer(), "bbbbbbbbbbbbbbb", arenaSize) == 0, charBuffer.rawBuffer() /*"Incorrect buffer"*/);

  // Ensure it did not overflow.
  auto extra = &supportBuffer[arenaSize];
  TEST_ASSERT_MESSAGE(memcmp(extra, "aaaaaaaaaaaaaaa", arenaSize) == 0, "Overflow detected");
}

static void test_overflowPushOrReplaceChar() {
  char supportBuffer[32];
  memcpy(supportBuffer, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", sizeof(supportBuffer));

  auto arenaSize = sizeof(supportBuffer) / 2;

  auto charBuffer = CharBuffer<SlabArena>(SlabArena<char>(supportBuffer, arenaSize));

  // Saturate the buffer
  while (charBuffer.pushOrReplaceChar('b')) {}

  TEST_ASSERT_MESSAGE(charBuffer.bufferLength() == arenaSize, "Bad arena size");

  for (auto i = 0; i < sizeof(supportBuffer); i++) {
    TEST_ASSERT_FALSE_MESSAGE(charBuffer.pushOrReplaceChar('b'), "Push error");
  }

  TEST_ASSERT_MESSAGE(charBuffer.bufferLength() == arenaSize, "Bad arena size");

  TEST_ASSERT_FALSE_MESSAGE(charBuffer.pushOrReplaceChar('\0'), "Push error");

  TEST_ASSERT_MESSAGE(charBuffer.bufferLength() == arenaSize, "Bad arena size");

  // Ensure that what should have been written is written.
  TEST_ASSERT_MESSAGE(memcmp(charBuffer.rawBuffer(), "bbbbbbbbbbbbbbb", arenaSize) == 0, supportBuffer);

  // Ensure it did not overflow.
  auto extra = &supportBuffer[arenaSize];
  TEST_ASSERT_MESSAGE(memcmp(extra, "aaaaaaaaaaaaaaa", arenaSize) == 0, supportBuffer);
}

static void test_underflowPop() {
  char supportBuffer[16];
  memcpy(supportBuffer, "aaaaaaaaaaaaaaa", sizeof(supportBuffer));

  auto arenaSize = sizeof(supportBuffer) / 2;
  auto offset = sizeof(supportBuffer) / 4;

  auto charBuffer = CharBuffer<SlabArena>(SlabArena<char>(&supportBuffer[offset], arenaSize));

  // Fill the buffer
  while (charBuffer.pushOrReplaceChar('b')) {}

  // Try empty the buffer by an excessive size.
  TEST_ASSERT_FALSE(charBuffer.popDestroyBy(arenaSize * 2));

  // Empty the buffer by an exact size.
  TEST_ASSERT(charBuffer.popDestroyBy(arenaSize));

  TEST_ASSERT(charBuffer.bufferLength() == 0);

  // Ensure the guards are still there.
  TEST_ASSERT(memcmp(supportBuffer, "aaaaaaaaaaaaaaa", offset) == 0);
  TEST_ASSERT(memcmp(&supportBuffer[offset + arenaSize], "aaa", sizeof(supportBuffer) - (offset + arenaSize)) == 0);

  // Ensure the right slice has been written to.
  TEST_ASSERT(memcmp(&supportBuffer[offset], "bbbbbbbb", arenaSize) == 0);
}


void runCharBufferTests() {
  RUN_TEST(test_simpleTest);
  RUN_TEST(test_emptyCharBuffer);
  RUN_TEST(test_pushString);
  RUN_TEST(test_available);
  RUN_TEST(test_overflowPushChar);
  RUN_TEST(test_overflowPushOrReplaceChar);
  RUN_TEST(test_underflowPop);
}