#include <unity.h>

#include <parse/SlabParser.hh>

#include <Util.hh>
#include <EventRecordHandle.hh>


/**
 * Array
 */

static void test_simpleArrayString() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(4, 1, VirtualHandleProxy(&handle));
  auto doc = "[ \"A2C\" ]";

  parseDocumentWith(parser, doc);

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL_MESSAGE(3, events->size(), "Bad event count");

  auto nameValueEvent = events->at(1);
  assertNameIndex(nameValueEvent.name, 0);
  assertValueString(nameValueEvent.value, "A2C");
}

static void test_simpleArrayInteger() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(4, 1, VirtualHandleProxy(&handle));
  auto doc = "[ 333 ]";

  parseDocumentWith(parser, doc);

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL_MESSAGE(3, events->size(), "Bad event count");

  auto nameValueEvent = events->at(1);
  assertNameIndex(nameValueEvent.name, 0);
  assertValueInteger(nameValueEvent.value, 333);
}

#if PJ_FLOATING_POINT_NUM_PARSE == 1
static void test_simpleArrayFloat() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(4, 1, VirtualHandleProxy(&handle));
  auto doc = "[ 2.4 ]";

  parseDocumentWith(parser, doc);

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL_MESSAGE(3, events->size(), "Bad event count");

  auto nameValueEvent = events->at(1);
  assertNameIndex(nameValueEvent.name, 0);
  assertValueFloat(nameValueEvent.value, 2.4f);
}
#endif

static void test_simpleArrayBool0() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(0, 1, VirtualHandleProxy(&handle));
  auto doc = "[ true ]";

  parseDocumentWith(parser, doc);

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL_MESSAGE(3, events->size(), "Bad event count");

  auto nameValueEvent = events->at(1);
  assertNameIndex(nameValueEvent.name, 0);
  assertValueBool(nameValueEvent.value, true);
}

static void test_simpleArrayBool1() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(0, 1, VirtualHandleProxy(&handle));
  auto doc = "[ false ]";

  parseDocumentWith(parser, doc);

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL_MESSAGE(3, events->size(), "Bad event count");

  auto nameValueEvent = events->at(1);
  assertNameIndex(nameValueEvent.name, 0);
  assertValueBool(nameValueEvent.value, false);
}

static void test_simpleArrayNull() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(0, 1, VirtualHandleProxy(&handle));
  auto doc = "[ null ]";

  parseDocumentWith(parser, doc);

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL_MESSAGE(3, events->size(), "Bad event count");

  auto nameValueEvent = events->at(1);
  assertNameIndex(nameValueEvent.name, 0);
  assertValueNull(nameValueEvent.value);
}

/**
 * Object
 */

static void test_simpleObjectString() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(4, 1, VirtualHandleProxy(&handle));
  auto doc = "{ \"a\" : \"A2C\" }";

  parseDocumentWith(parser, doc);

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL_MESSAGE(3, events->size(), "Bad event count");

  auto nameValueEvent = events->at(1);
  assertNameKey(nameValueEvent.name, "a");
  assertValueString(nameValueEvent.value, "A2C");
}

static void test_simpleObjectInteger() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(2, 1, VirtualHandleProxy(&handle));
  auto doc = "{ \"a\" : 1 }";

  parseDocumentWith(parser, doc);

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL_MESSAGE(3, events->size(), "Bad event count");

  auto nameValueEvent = events->at(1);
  assertNameKey(nameValueEvent.name, "a");
  assertValueInteger(nameValueEvent.value, 1);
}

#if PJ_FLOATING_POINT_NUM_PARSE == 1
static void test_simpleObjectFloat() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(4, 1, VirtualHandleProxy(&handle));
  auto doc = "{ \"a\" : 3.7 }";

  parseDocumentWith(parser, doc);

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL_MESSAGE(3, events->size(), "Bad event count");

  auto nameValueEvent = events->at(1);
  assertNameKey(nameValueEvent.name, "a");
  assertValueFloat(nameValueEvent.value, 3.7);
}
#endif

static void test_simpleObjectBool0() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(2, 1, VirtualHandleProxy(&handle));
  auto doc = "{ \"a\" : true }";

  parseDocumentWith(parser, doc);

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL_MESSAGE(3, events->size(), "Bad event count");

  auto nameValueEvent = events->at(1);
  assertNameKey(nameValueEvent.name, "a");
  assertValueBool(nameValueEvent.value, true);
}

static void test_simpleObjectBool1() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(2, 1, VirtualHandleProxy(&handle));
  auto doc = "{ \"a\" : false }";

  parseDocumentWith(parser, doc);

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL_MESSAGE(3, events->size(), "Bad event count");

  auto nameValueEvent = events->at(1);
  assertNameKey(nameValueEvent.name, "a");
  assertValueBool(nameValueEvent.value, false);
}

static void test_simpleObjectNull() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(2, 1, VirtualHandleProxy(&handle));
  auto doc = "{ \"a\" : null }";

  parseDocumentWith(parser, doc);

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL_MESSAGE(3, events->size(), "Bad event count");

  auto nameValueEvent = events->at(1);
  assertNameKey(nameValueEvent.name, "a");
  assertValueNull(nameValueEvent.value);
}


void runSimpleNameValueTest() {
  RUN_TEST(test_simpleArrayString);
  RUN_TEST(test_simpleArrayInteger);
  RUN_TEST(test_simpleArrayBool0);
  RUN_TEST(test_simpleArrayBool1);
  RUN_TEST(test_simpleArrayNull);

  RUN_TEST(test_simpleObjectString);
  RUN_TEST(test_simpleObjectInteger);
  RUN_TEST(test_simpleObjectBool0);
  RUN_TEST(test_simpleObjectBool1);
  RUN_TEST(test_simpleObjectNull);

#if PJ_FLOATING_POINT_NUM_PARSE == 1
  RUN_TEST(test_simpleArrayFloat);
  RUN_TEST(test_simpleObjectFloat);
#endif
}

void setUp() {
  // This can be empty. Unity requires these.
}

void tearDown() {
  // This can be empty. Unity requires these.
}

int main() {
  UNITY_BEGIN();
  runSimpleNameValueTest();
  return UNITY_END();
}
