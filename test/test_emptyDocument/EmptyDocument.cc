#include <unity.h>

#include <PicoJson.hh>

#include <Util.hh>
#include <EventRecordHandle.hh>


class SingleStructureHandle {
private:
  pj::JsonStructure expected;
  bool errored;
  int8_t _nesting;
public:
  explicit SingleStructureHandle(pj::JsonStructure expected) : expected(expected), errored(false), _nesting(0) {}

  bool isErrored() {
    return this->errored;
  }

  int8_t nesting() {
    return this->_nesting;
  }


  void onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth) {
    if (depth == this->_nesting && structureType == this->expected && !this->errored) {
      _nesting++;
    } else {
      this->errored = true;
      TEST_FAIL_MESSAGE("Bad structure");
    }
  }

  void onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth) {
    if (this->_nesting == 0 && this->_nesting == depth) {
      this->errored = true;
      TEST_FAIL_MESSAGE("Ending structure before there was any");
    } else if (this->_nesting < 0) {
      this->errored = true;
      TEST_FAIL_MESSAGE("Negative nesting");
    } else if (!this->errored) {
      this->_nesting--;
    }
  }

  void onKey(const char* const key) {
    this->errored = true;
    TEST_FAIL_MESSAGE("Invoked key when there wasn't any");
  }

  void onValue(const pj::JsonValue& value) {
    this->errored = true;
    TEST_FAIL_MESSAGE("Invoked value when there wasn't any");
  }
};


static void emptyStructureDocument(const char* const doc, const pj::JsonStructure structure) {
  auto parser = pj::SlabParser<SingleStructureHandle>(1, 1, SingleStructureHandle(structure));

  for (auto i = 0; doc[i] != '\0'; i++) {
    const auto err = parser.parse(doc[i]);
    TEST_ASSERT_EQUAL(pj::ParseError::None, err);
  }

  SingleStructureHandle& handle = parser.handle();

  TEST_ASSERT(!handle.isErrored());

  TEST_ASSERT_EQUAL(0, handle.nesting());
}


static void test_emptyArrayDocument() {
  emptyStructureDocument("[]", pj::JsonStructure::Array);
  emptyStructureDocument("[ ]", pj::JsonStructure::Array);
  emptyStructureDocument("[\n\t \r]", pj::JsonStructure::Array);
}

static void test_emptyObjectDocument() {
  emptyStructureDocument("{}", pj::JsonStructure::Object);
  emptyStructureDocument("{ }", pj::JsonStructure::Object);
  emptyStructureDocument("{\n\t \r}", pj::JsonStructure::Object);
}


void runEmptyDocumentTest() {
  RUN_TEST(test_emptyArrayDocument);
  RUN_TEST(test_emptyObjectDocument);
}

void setUp() {
  // This can be empty. Unity requires these.
}

void tearDown() {
  // This can be empty. Unity requires these.
}

int main() {
  UNITY_BEGIN();
  runEmptyDocumentTest();
  return UNITY_END();
}
