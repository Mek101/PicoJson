add_executable(EmptyDocumentTest
    ${CMAKE_CURRENT_SOURCE_DIR}/EmptyDocument.cc
)

target_link_libraries(EmptyDocumentTest
    ${PROJECT_NAME}
    unity
    TestHelpers
)

add_test(EmptyDocument EmptyDocumentTest)
