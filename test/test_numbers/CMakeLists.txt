add_executable(NumbersTest
    ${CMAKE_CURRENT_SOURCE_DIR}/Numbers.cc
)

target_link_libraries(NumbersTest
    ${CMAKE_PROJECT_NAME}
    unity
    TestHelpers
)

add_test(Numbers NumbersTest)
