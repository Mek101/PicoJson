#include <vector>
#include <stdint.h>

#include <unity.h>

#include <parse/DynParser.hh>

#include <EventRecordHandle.hh>
#include <Util.hh>


auto goodJsonIntegers = R"([
  123,
  -123
])";

auto goodJsonFloats = R"([
  123.456,
  -123.456,
  12e34,
  12e-34,
  12e+34,
  12E34,
  12E-34,
  12E+34,
  12.34e12,
  12.34e-12,
  12.34e+12,
  12.34E12,
  12.34E-12,
  12.34E+12
])";

// clang-format off
int32_t goodIntegers[] = {
  123,
  -123
};

// clang-format off
float goodFloats[] = {
  123.456,
  -123.456,
  12e34,
  12e-34,
  12e+34,
  12E34,
  12E-34,
  12E+34,
  12.34e12,
  12.34e-12,
  12.34e+12,
  12.34E12,
  12.34E-12,
  12.34E+12
};


static void assertEventAtIsArrayBegin(std::vector<Record>* events, size_t index) {
  TEST_ASSERT_MESSAGE(events->at(index).value.isStructureBegin(), "Not a begin");
  TEST_ASSERT_MESSAGE(*events->at(index).value.getStructureBegin() == pj::JsonStructure::Array, "Not an array");
}

static void assertEventAtIsArrayEnd(std::vector<Record>* events, size_t index) {
  TEST_ASSERT_MESSAGE(events->at(index).value.isStructureEnd(), "Not an end");
  TEST_ASSERT_MESSAGE(*events->at(index).value.getStructureEnd() == pj::JsonStructure::Array, "Not an array");
}


static void assertEventAtIsArrayInteger(std::vector<Record>* events, size_t index, int32_t expectIndex, int32_t expectInteger) {
  TEST_ASSERT(events->at(index).name.isArrayIndex() && *events->at(index).name.getIndex() == expectIndex);
  assertValueInteger(events->at(index).value, expectInteger);
}

static void assertEventAtIsArrayFloat(std::vector<Record>* events, size_t index, int32_t expectIndex, float expectFloat) {
  TEST_ASSERT(events->at(index).name.isArrayIndex() && *events->at(index).name.getIndex() == expectIndex);
  assertValueFloat(events->at(index).value, expectFloat);
}


static void test_parseSimpleNumber() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(30, 1, VirtualHandleProxy(&handle));

  auto doc = "[ 150 ]";

  parseDocumentWith(parser, doc);

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL(3, events->size());

  assertEventAtIsArrayBegin(events, 0);
  assertEventAtIsArrayInteger(events, 1, 0, 150);
  assertEventAtIsArrayEnd(events, 2);
}

static void test_parseIntegers() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(30, 1, VirtualHandleProxy(&handle));

  parseDocumentWith(parser, goodJsonIntegers);

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL(4, events->size());

  assertEventAtIsArrayBegin(events, 0);
  assertEventAtIsArrayInteger(events, 1, 0, goodIntegers[0]);
  assertEventAtIsArrayInteger(events, 2, 1, goodIntegers[1]);
  assertEventAtIsArrayEnd(events, 3);
}

#if PJ_FLOATING_POINT_NUM_PARSE == 1

static void test_parseFloats() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(30, 1, VirtualHandleProxy(&handle));

  parseDocumentWith(parser, goodJsonFloats);

  auto events = handle.getEvents();

  TEST_ASSERT_EQUAL(16, events->size());

  assertEventAtIsArrayBegin(events, 0);
  for (auto i = 1; i < 1; i++) {
    assertEventAtIsArrayFloat(events, i, i - 1, goodFloats[i - 1]);
  }
  assertEventAtIsArrayEnd(events, 15);
}

#else

static void test_parseFloats() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(30, 1, VirtualHandleProxy(&handle));

  pj::ParseError err = pj::ParseError::None;

  for (auto i = 0; goodJsonFloats[i] != '\0' && err == pj::ParseError::None; i++) {
    auto c = goodJsonFloats[i];
    err = parser.parse(c);
  }

  TEST_ASSERT_FALSE(handle.isErrored());
  TEST_ASSERT_MESSAGE(err == pj::ParseError::BadNumber, errorToString(err));
}

#endif

static void test_parseBadNaN() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(30, 1, VirtualHandleProxy(&handle));

  auto doc = "[ NaN ]";

  pj::ParseError err = pj::ParseError::None;

  for (auto i = 0; i < 3; i++) {
    err = parser.parse(doc[i]);
  }

  TEST_ASSERT_FALSE(handle.isErrored());
  TEST_ASSERT_MESSAGE(err == pj::ParseError::BadValue, errorToString(err));
}

static void test_parseBadSignedNaN() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(30, 1, VirtualHandleProxy(&handle));

  const char doc[] = "[ -NaN ]";

  pj::ParseError err = pj::ParseError::None;

  for (auto i = 0; i < sizeof(doc) && err == pj::ParseError::None; i++) {
    err = parser.parse(doc[i]);
  }

  TEST_ASSERT_FALSE(handle.isErrored());
  TEST_ASSERT_MESSAGE(err == pj::ParseError::BadNumber, errorToString(err));
}

static void test_parseBadNumber() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(30, 1, VirtualHandleProxy(&handle));

  const char doc[] = "[ -NotANumber ]";

  pj::ParseError err = pj::ParseError::None;

  for (auto i = 0; i < sizeof(doc) && err == pj::ParseError::None; i++) {
    err = parser.parse(doc[i]);
  }

  TEST_ASSERT_FALSE(handle.isErrored());
  TEST_ASSERT_MESSAGE(err == pj::ParseError::BadNumber, errorToString(err));
}

/**
 * Json numbers may not start with a plus, only a minus.
 */
static void test_parseBadSignedNumber() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(30, 1, VirtualHandleProxy(&handle));

  const char doc[] = "[ +18 ]";

  pj::ParseError err = pj::ParseError::None;

  for (auto i = 0; i < sizeof(doc) && err == pj::ParseError::None; i++) {
    err = parser.parse(doc[i]);
  }

  TEST_ASSERT_FALSE(handle.isErrored());
  TEST_ASSERT_MESSAGE(err == pj::ParseError::BadValue, errorToString(err));
}

static void test_integerOverflow() {
  auto handle = EventRecordHandle();
  auto parser = pj::SlabParser<VirtualHandleProxy>(50, 1, VirtualHandleProxy(&handle));

  const char doc[] = "[ 8999999999999999999999999999999999999999999 ]";

  pj::ParseError err = pj::ParseError::None;

  for (auto i = 0; i < sizeof(doc); i++) {
    auto err = parser.parse(doc[i]);
    TEST_ASSERT_MESSAGE(err == pj::ParseError::None, errorToString(err));
  }

  TEST_ASSERT_FALSE(handle.isErrored());

  auto events = handle.getEvents();
  TEST_ASSERT_EQUAL(3, events->size());

  assertEventAtIsArrayBegin(events, 0);
  assertEventAtIsArrayInteger(events, 1, 0, INT32_MAX);
  assertEventAtIsArrayEnd(events, 2);
}


void runNumbersTest() {
  RUN_TEST(test_parseSimpleNumber);
  RUN_TEST(test_parseIntegers);
  RUN_TEST(test_parseFloats);
  RUN_TEST(test_parseBadNaN);
  RUN_TEST(test_parseBadSignedNaN);
  RUN_TEST(test_parseBadNumber);
  RUN_TEST(test_parseBadSignedNumber);
  RUN_TEST(test_integerOverflow);
}

void setUp() {
  // This can be empty. Unity requires these.
}

void tearDown() {
  // This can be empty. Unity requires these.
}

int main() {
  UNITY_BEGIN();
  runNumbersTest();
  return UNITY_END();
}
