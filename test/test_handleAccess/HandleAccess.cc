#include <unity.h>

#include <PicoJson.hh>

#include <DummyHandle.hh>


static void test_static() {
  auto parser = pj::StaticParser<10, 3, DummyHandle>(DummyHandle());

  DummyHandle& hRef = parser.handle();
}

static void test_slab() {
  auto parser = pj::SlabParser<DummyHandle>(10, 3, DummyHandle());

  DummyHandle& hRef = parser.handle();
}

static void test_dyn() {
  auto parser = pj::DynParser<DummyHandle>(10, 3, DummyHandle());

  DummyHandle& hRef = parser.handle();
}


static void runHandleAccess() {
  RUN_TEST(test_static);
  RUN_TEST(test_slab);
  RUN_TEST(test_dyn);
}

void setUp() {
  // This can be empty. Unity requires these.
}

void tearDown() {
  // This can be empty. Unity requires these.
}

int main() {
  UNITY_BEGIN();
  runHandleAccess();
  return UNITY_END();
}
