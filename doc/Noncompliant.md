# Known noncompliant behavior

A document to track the parser known noncompliant behavior that won't be considered a bug.

 - The terminator character (`\0`) is always ignored by the parsers `parse()` methods.
 - Any terminator character escaped as a unicode codepoint (`\u0000`) is always discarded.
 - Numbers with lending 0's are legal with the non-libc parser.
