# Arena API

The arena API is dedicated to providing a generic way to access a slice of memory to work with.

Here in pseudocode.

```cpp
interface Arena<T> {
    /**
     * Returns a reference to the element at the given index. May be up to available() - 1.
     */
    T& operator[](const uint16_t index);

    /**
     * Returns the number of bytes the user may use.
     */
    uint16_t available();

    /**
     * Used to request at least size bytes from the arena to be available.
     * Returns true if at least the requested size is available to the user. If false, the arena is unchanged.
     */
    bool setAvailable(const uint16_t size);

    /**
     * Returns a pointer to the slice of available elements. See available() for the buffer length.
     * May be null if there are no elements available.
     * The buffer's lifetime extends up to the next setAvailable() call returning true.
     */
    T* rawBuffer();

    /**
     * Returns the total capacity of the arena, and the maximum number of elements that the user may request to be available for use.
     */
    uint16_t capacity();
}
```