# The Handle API

The consumer code must provide a handle object (or type) to receive parsing events as the parser class processes the
json input.
The handle must be a consumer supplied type with the following API:

```cpp
class Handle {
public:
  /**
   * Called upon the beginning of a structure.
   *
   * The json format allows arbitrary nesting of structures, which may either begin after an object's key or as an
   * element in an array. If the client code needs to know the "path" to the current location in the document, or the
   * type of the current structure, it needs to keep track of it.
   * The structureType parameter indicates the type of the json structure: either an array or an object.
   * The depth parameter indicates the structure's depth or nesting level, with the root structure having a depth of 0.
   */
  void onStructureBegin(const pj::JsonStructure structureType, const uint16_t depth)

  /**
   * Called upon the end of the current structure.
   * The depth parameter indicates the structure's depth or nesting level, with the root structure having a depth of 0.
   */
  void onStructureEnd(const pj::JsonStructure structureType, const uint16_t depth)

  /**
   * Called upon a json's object key being parsed.
   *
   * In a correct document, it will be immediately followed by a call to onValue or onStructureBegin.
   * It's never called when the current structure is an array.
   * Since the parser conserves no memory of past keys, this method may be invoked multiple times with the same key.
   * The key parameter is only valid within this method call. If the consumer code needs to access the key outside of
   * the method, then it must make a copy on a separate buffer.
   */
  void onKey(const char* const key)

  /**
   * Called upon a json value being parsed.
   *
   * If the current structure is an object, it is always preceded by a call to onKey, if it's an array, it will
   * not be preceded by any call.
   * The value parameter is only valid within this method call. If the consumer code needs to access the value outside
   * of the method, then it must make a copy of the effective value.
   */
  void onValue(const pj::JsonValue& value)
};
```

With `JsonStructure` being either an Array or an Object, and `JsonValue` holding either a string, a 32-bit integer, a
32-bit floating point number, a boolean value or a json null value. 

As a general rule, to keep memory usage low, the parser conserves little to no state of the document being parsed, and
as such the client code should internally keep track of whatever state it needs, updating it on events.
