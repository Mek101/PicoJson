# The Parser API

All parsers present these two methods as their main API:

```cpp
/**
 * Advances the parser state with the given character. The terminator character is always ignored.
 * Returns an error indicating the parser's state.
 */
ParseError parse(const char c)

/**
 * Resets the parser to it's initial empty state.
 */
void reset()
```

Calling either methods inside the handle provided to the same parser is not supported and may result in undefined behavior.

The `ParseError` enumeration returned by the `parse()` method returns the current state of the parser after advancing it with a character.
There are three cases:

- The error is `None`: the parser is in a correct state and may continue parsing.

- The error is a non-fatal error, either `StringOverflow`, `UnescapedControlCharacter` which results in the current string value being truncated or a character being ignored.
Those errors are "safe", and while they may indicate an erroneous input, the parser is still in a correct state and can continue parsing. The consumer code should either be able to safely handle truncated strings and/or missing control characters, or should discard the document and reset the parser.

- The error is fatal: this is either caused by malformed input or exhaustion of the resources allocated to the parser.
In either case, the parser will enter an "errored" state as it cannot continue parsing safely. The current document must be discarded and the parser reset.


# Included Parsers

MicroJson includes the following parsers for consumer code usage:

 - `StaticParser<CHAR_BUFFER_SIZE, MAX_DEPTH, HANDLE>(handle)`: a parser with inlined buffers, accepting strings, numbers and keys up to `CHAR_BUFFER_SIZE` long,
   with a maximum structure nesting depth of `MAX_DEPTH`.
   The inline buffers should make it an ideal candidate for compiler optimization, in exchange for making the structure bigger and slower to copy or move
   around. The buffers size being part of the type make it impossible to mix parsers with differently sized buffers.

 - `SlabParser<HANDLE>(charBufferSize, maxDepth, handle)`: a parser with a heap allocated, non-resizable buffer. Accepts strings, numbers and keys up to
   `charBufferSize` long, and a maximum structure nesting depth of `maxDepth`.
   The non-resizable heap allocated buffer makes the structure lightweight enough to be easily copied and moved around without contributing to memory
   fragmentation.

 - `DynParser<HANDLE>(charBufferSize, maxDepth, handle)`: a parser with two heap allocated resizable buffers. Accepts strings, numbers and keys up to
   `charBufferSize` long, and a maximum structure nesting depth of `maxDepth`.
   The two resizable buffers make the parser suited for json documents with rare long strings or rare deeply nested structures in applications not
   concerned by memory fragmentation.
