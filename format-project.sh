#!/bin/sh
find "${PWD}/" -iname '*.hh' -o -iname '*.cc' | xargs clang-format -i
