#include <util/arena/DynArenaImpl.hh>

constexpr uint16_t clamp(const uint16_t num, const uint16_t min, const uint16_t max) {
  return num > max ? max : (num < min ? min : num);
}

constexpr uint16_t clampAtLeast(const uint16_t num, const uint16_t min) {
  return num < min ? min : num;
}

bool PJ_IMPL_NAMESPACE::DynArenaImpl::resizeAlloc(uint16_t size, const size_t objSize) noexcept {
  // SAFETY: size * objSize is checked for overflow by the constructor. size is never 0.
  void* newStart = realloc(this->impl.buffer, size * objSize);
  if (newStart == nullptr) {
    return false;
  }

  this->impl.buffer = newStart;
  this->impl.size = size;
  return true;
}

PJ_IMPL_NAMESPACE::DynArenaImpl::DynArenaImpl(PJ_IMPL_NAMESPACE::DynArenaImpl&& other) noexcept {
  this->maxCount = other.maxCount;
  this->minCount = other.minCount;

  this->impl = other.impl;
  other.impl.buffer = nullptr;
  other.impl.size = 0;
}

bool PJ_IMPL_NAMESPACE::DynArenaImpl::setAvailable(const uint16_t size, const size_t objSize) noexcept {
  if (size > this->maxCount) {
    return false;
  }

  const auto currentAvailableSize = this->impl.size;

  // Augment the allocation by 50% if the available size is insufficient.
  if (size > currentAvailableSize) {
    const auto allocSize = clamp(size + (size >> 1), this->minCount, this->maxCount);
    return this->resizeAlloc(allocSize, objSize);
  }
  // Resize to 75% if only half of the allocated space is to be used.
  else if (size <= (currentAvailableSize >> 1)) {
    const auto allocSize = clampAtLeast((currentAvailableSize >> 1) + (currentAvailableSize >> 2), this->minCount);
    if (allocSize != currentAvailableSize) {
      // Shrinking is infallible.
      this->resizeAlloc(allocSize, objSize);
    }
  }

  return true;
}
