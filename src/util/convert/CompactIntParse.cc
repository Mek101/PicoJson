#if PJ_LIBC_NUM_PARSE == 0 && PJ_FLOATING_POINT_NUM_PARSE == 0

#include <util/convert/NumParse.hh>

#include <stdint.h>

#include <util/Namespace.hh>
#include <util/WellKnown.hh>
#include <util/Polyfills.hh>


static PJ_FORCE_INLINE PJ_IMPL_NAMESPACE::NumParseResult buildInvalidResult() {
  PJ_IMPL_NAMESPACE::NumParseResult r;
  r.err = PJ_IMPL_NAMESPACE::NumParseError::Invalid;
  return r;
}

static PJ_FORCE_INLINE PJ_IMPL_NAMESPACE::NumParseResult buildOverflownIntegerResult(const bool isNegative) {
  PJ_IMPL_NAMESPACE::NumParseResult r;
  r.iValue = isNegative ? INT32_MIN : INT32_MAX;
  r.err = PJ_IMPL_NAMESPACE::NumParseError::IntegerOverflow;
  return r;
}

static PJ_FORCE_INLINE PJ_IMPL_NAMESPACE::NumParseResult buildIntegerResult(const uint32_t accumulator, const bool isNegative) {
  PJ_IMPL_NAMESPACE::NumParseResult r;
  r.err = PJ_IMPL_NAMESPACE::NumParseError::IntegerNone;
  r.iValue = isNegative ? -accumulator : accumulator;
  return r;
}

PJ_IMPL_NAMESPACE::NumParseResult PJ_IMPL_NAMESPACE::tryParseNumber(const char *str) noexcept {
  // Is the main number sign negative?
  bool isNegative = false;
  uint32_t integerMax = INT32_MAX;  // The maximum value of the integer. Depends on the sign.

  // The possible results.
  uint32_t integerResult = 0;

  // Evaluate the sign.
  if (*str == '-') {
    isNegative = true;
    integerMax += 1;  // signed integers in 2-complement machines are asymmetric.
    str++;
  }

  // Check that there's at least 1 digit.
  if (!isDigit(*str)) {
    return buildInvalidResult();
  }

  // Convert the integer part.
  do {
    const uint8_t digitValue = *str++ - '0';
    integerResult *= 10;

    // Check for future overflows
    if (integerResult > (integerMax - digitValue)) {
      return buildOverflownIntegerResult(isNegative);
    }

    integerResult += digitValue;
  } while (isDigit(*str));

  if (*str != '\0') {
    return buildInvalidResult();
  }

  // Does not have a divisor or exponent: it's an integer.
  return buildIntegerResult(integerResult, isNegative);
}

#endif
