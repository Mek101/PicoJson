#if PJ_LIBC_NUM_PARSE == 1 && PJ_FLOATING_POINT_NUM_PARSE == 0

#include <util/convert/NumParse.hh>

#include <util/Polyfills.hh>

#include <stddef.h>
#include <errno.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <stdlib.h>


PJ_IMPL_NAMESPACE::NumParseResult PJ_IMPL_NAMESPACE::tryParseNumber(const char* str) noexcept {
  static_assert(sizeof(int32_t) <= sizeof(long), "strtol too limited to fit a uint32_t");
  static_assert(INT32_MAX <= LONG_MAX, "strtol too limited to fit a uint32_t");
  static_assert(INT32_MIN >= LONG_MIN, "strtol too limited to fit a uint32_t");

  // Reset errno.
  errno = 0;

  char* end;
  const long res_long = strtol(str, &end, 10);

  const auto err = errno;

  NumParseResult r;

  if (*end != '\0') {
    r.err = NumParseError::Invalid;
    return r;
  }

  if (err == ERANGE) {
    r.err = NumParseError::IntegerOverflow;
    r.iValue = res_long > 0 ? INT32_MAX : INT32_MIN;
    return r;
  }

  if (res_long > INT32_MAX) {
    r.err = NumParseError::IntegerOverflow;
    r.iValue = INT32_MAX;
  } else if (res_long < INT32_MIN) {
    r.err = NumParseError::IntegerOverflow;
    r.iValue = INT32_MIN;
  } else {
    r.err = NumParseError::IntegerNone;
    r.iValue = res_long;
  }

  return r;
}

#endif
