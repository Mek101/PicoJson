#if PJ_LIBC_NUM_PARSE == 0 && PJ_FLOATING_POINT_NUM_PARSE == 1

#include <util/convert/NumParse.hh>

#include <stdint.h>
#include <float.h>
#include <math.h>

#include <util/Namespace.hh>
#include <util/WellKnown.hh>
#include <util/Polyfills.hh>

#if defined(ARDUINO)
#define OVERFLOW_VALUE INFINITY
#else
#define OVERFLOW_VALUE HUGE_VALF
#endif


static PJ_FORCE_INLINE PJ_IMPL_NAMESPACE::NumParseResult buildInvalidResult() {
  PJ_IMPL_NAMESPACE::NumParseResult r;
  r.err = PJ_IMPL_NAMESPACE::NumParseError::Invalid;
  return r;
}

static PJ_FORCE_INLINE PJ_IMPL_NAMESPACE::NumParseResult buildOverflownIntegerResult(const bool isNegative) {
  PJ_IMPL_NAMESPACE::NumParseResult r;
  r.iValue = isNegative ? INT32_MIN : INT32_MAX;
  r.err = PJ_IMPL_NAMESPACE::NumParseError::IntegerOverflow;
  return r;
}

static PJ_FORCE_INLINE PJ_IMPL_NAMESPACE::NumParseResult buildIntegerResult(const uint32_t accumulator, const bool isNegative) {
  PJ_IMPL_NAMESPACE::NumParseResult r;
  r.err = PJ_IMPL_NAMESPACE::NumParseError::IntegerNone;
  r.iValue = isNegative ? -accumulator : accumulator;
  return r;
}

static PJ_FORCE_INLINE PJ_IMPL_NAMESPACE::NumParseResult buildOutOfRangeFloatResult(const bool isUnderflow, const bool isNegative) {
  PJ_IMPL_NAMESPACE::NumParseResult r;

  if (isUnderflow) {
    r.err = PJ_IMPL_NAMESPACE::NumParseError::FloatUnderflow;
    r.fValue = isNegative ? -0.0f : 0.0f;
  } else {
    r.err = PJ_IMPL_NAMESPACE::NumParseError::FloatOverflow;
    r.fValue = isNegative ? -OVERFLOW_VALUE : OVERFLOW_VALUE;
  }

  return r;
}

static PJ_FORCE_INLINE PJ_IMPL_NAMESPACE::NumParseResult buildFloatResult(const float value) {
  PJ_IMPL_NAMESPACE::NumParseResult r;
  r.err = PJ_IMPL_NAMESPACE::NumParseError::FloatNone;
  r.fValue = value;
  return r;
}


PJ_IMPL_NAMESPACE::NumParseResult PJ_IMPL_NAMESPACE::tryParseNumber(const char *str) noexcept {
  // Is the main number sign negative?
  bool isNegative = false;
  uint32_t integerMax = INT32_MAX;  // The maximum value of the integer. Depends on the sign

  // The possible results.
  float floatResult = 0.0f;
  uint32_t integerResult = 0;

  // If it has a divisor or exponent, it's a floating number.
  bool hasDecimalOrExponent = false;

  // Evaluate the sign.
  if (*str == '-') {
    isNegative = true;
    integerMax += 1;  // signed integers in 2-complement machines are asymmetric.
    str++;
  }

  // Check that there's at least 1 digit.
  if (!isDigit(*str)) {
    return buildInvalidResult();
  }

  // Convert the integer part.
  do {
    const uint8_t digitValue = *str++ - '0';
    integerResult *= 10;

    // Check for future overflows
    if (integerResult > (integerMax - digitValue)) {
      return buildOverflownIntegerResult(isNegative);
    }

    integerResult += digitValue;
  } while (isDigit(*str));

  // Detect floating point & mantissa.
  if (*str == '.') {
    float divisor = 1.0f;

    // Cast the value type.
    floatResult = integerResult;
    hasDecimalOrExponent = true;

    str++;
    while (isDigit(*str)) {
      const float fVal = *str++ - '0';
      divisor *= 10.0f;
      // As we go left, the precision becomes more uncertain anyway
      floatResult += fVal / divisor;
    }
  }


  // Detect the exponent.
  uint16_t power = 0;
  // Power sign multiplier.
  bool isPowerNegative = false;
  if ((*str & 0x5F) == 'E') {
    if (!hasDecimalOrExponent) {
      // Cast the value type that wasn't cast before.
      floatResult = integerResult;
      hasDecimalOrExponent = true;
    }

    str++;

    // Detect the exponent's sign.
    uint16_t powerMax = FLT_MAX_EXP;
    if (*str == '-') {
      isPowerNegative = true;
      powerMax = -FLT_MIN_EXP;
      str++;
    } else if (*str == '+') {
      str++;
    }

    while (isDigit(*str)) {
      const uint8_t digitValue = *str++ - '0';
      power *= 10.0f;

      if ((powerMax - digitValue) > power) {
        // Float overflow/underflow detected.
        return buildOutOfRangeFloatResult(isPowerNegative, isNegative);
      }

      power += digitValue;
    }
  }

  if (*str != '\0') {
    return buildInvalidResult();
  }

  if (hasDecimalOrExponent) {
    // Is a floating point number for sure.
    // Apply the sign.
    if (isNegative) {
      floatResult *= -1.0f;
    }

    // Adjust result on exponent sign.
    if (isPowerNegative) {
      for (; power != 0; power--) {
        floatResult /= 10.0f;
      }
    } else {
      for (; power != 0; power--) {
        floatResult *= 10.0f;
      }
    }

    return buildFloatResult(floatResult);
  }

  // Does not have a divisor or exponent: it's an integer.
  return buildIntegerResult(integerResult, isNegative);
}

#endif
