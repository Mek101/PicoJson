#if PJ_ESCAPED_UNICODE == 1

#include <util/convert/UTF16Parse.hh>

#include <util/Polyfills.hh>

#if (defined(__BYTE_ORDER__) && (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)) || defined(__BIG_ENDIAN__) || defined(_BIG_ENDIAN)
#error "Unsupported platform. Implement big-endian decoding logic"
#else

/**
 * CORRECTNESS: codeunit must be a valid UTF codeunit.
 * SAFETY: buff must have a length >= 4.
 */
static PJ_FORCE_INLINE int8_t writeCodeunit(char* buff, int32_t codeunit) {
  int8_t res = 0;

  // The terminator is always elided.
  if (codeunit == 0x0000) {
    res = 0;
  } else if (codeunit < 0x0080) {
    buff[0] = (char)codeunit;
    res = 1;
  } else if (codeunit < 0x0800) {
    buff[0] = (char)(((codeunit >> 6) & 0x1F) | 0xC0);
    buff[1] = (char)((codeunit & 0x3F) | 0x80);
    res = 2;
  } else if (codeunit < 0x10000) {
    buff[0] = (char)(((codeunit >> 12) & 0x0F) | 0xE0);
    buff[1] = (char)(((codeunit >> 6) & 0x3F) | 0x80);
    buff[2] = (char)((codeunit & 0x3F) | 0x80);
    res = 3;
  } else if (codeunit < 0x110000) {
    buff[0] = (char)(((codeunit >> 18) & 0x07) | 0xF0);
    buff[1] = (char)(((codeunit >> 12) & 0x3F) | 0x80);
    buff[2] = (char)(((codeunit >> 6) & 0x3F) | 0x80);
    buff[3] = (char)((codeunit & 0x3F) | 0x80);
    res = 4;
  }

  return res;
}

#endif

static uint16_t parseHex4Unchecked(const char* const hexCharBuffer) {
  uint16_t result = 0;
  int_fast8_t i = 4;

  do {
    i--;
    const char c = hexCharBuffer[i];

    int8_t value;

    if (c <= '9') {
      // Assume it's >= '0'.
      value = c - '0';
    } else {
      // Assume hex ascii characters.
      // Normalize it to uppercase with a bitmask, then subtract 'a' + 10.
      value = (c & 0x5F) - 55;
    }

    result = (result << 4) | value;
  } while(i != 0);

  return result;
}


PJ_IMPL_NAMESPACE::UTF16ConversionResult PJ_IMPL_NAMESPACE::tryConvertInplace(char* buff, const bool isPairs) noexcept {
  PJ_IMPL_NAMESPACE::UTF16ConversionResult res;

  uint32_t codeunit = parseHex4Unchecked(buff);

  // Is it a surrogate codepoint?
  if (codeunit >= 0xD800 && codeunit < 0xDC00) {
    if (!isPairs) {
      res.err = UTF16ConversionError::IncompletePair;
      return res;
    }

    // High surrogates go first. Assume it is a high surrogate.
    const auto hs = codeunit;
    const auto ls = parseHex4Unchecked(buff + 4);

    if (ls >= 0xDC00 && ls <= 0xDFFF) {
        codeunit = ((hs - 0xD800) << 10) + (ls - 0xDC00) + 0x10000;
    } else {
        codeunit = 0xFFFD;
    }
  }

  if (codeunit >= 0x110000) {
    // Invalid codepoint.
    codeunit = 0xFFFD;
    res.err = PJ_IMPL_NAMESPACE::UTF16ConversionError::InvalidCodepoint;
  } else {
    res.err = PJ_IMPL_NAMESPACE::UTF16ConversionError::None;
  }

  res.written = writeCodeunit(buff, codeunit);

  return res;
}

#endif
