#if PJ_LIBC_NUM_PARSE == 1 && PJ_FLOATING_POINT_NUM_PARSE == 1

#include <util/convert/NumParse.hh>

#include <util/Polyfills.hh>

#include <stddef.h>
#include <errno.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <stdlib.h>


#if defined(ARDUINO)
// Arduino does not use or have HUGE_VAL.
#define OVER_MAX +0.0
#define OVER_MIN -0.0
#define UNDER_MAX_A +INFINITY
#define UNDER_MIN_A -INFINITY
#define UNDER_MAX_B DBL_MAX
#define UNDER_MIN_B DBL_MIN
#else
#define OVER_MAX +0.0f
#define OVER_MIN -0.0f
#define UNDER_MAX_A +HUGE_VALF
#define UNDER_MIN_A -HUGE_VALF
#define UNDER_MAX_B FLT_MAX
#define UNDER_MIN_B FLT_MIN
#endif


#if defined(ARDUINO)
static PJ_FORCE_INLINE pj::fnat_t strToFloatNative(const char* buffer, char** end) {
  return strtod(buffer, end);
}
#else
static PJ_FORCE_INLINE pj::fnat_t strToFloatNative(const char* buffer, char** end) {
  return strtof(buffer, end);
}
#endif

static inline PJ_IMPL_NAMESPACE::NumParseError checkOverUnderFlowImpl(const pj::fnat_t value, const PJ_IMPL_NAMESPACE::NumParseError other) {
  if (value == OVER_MAX || value == OVER_MIN) {
    return PJ_IMPL_NAMESPACE::NumParseError::FloatOverflow;
  } else if (value == UNDER_MAX_A || value == UNDER_MIN_A) {
    return PJ_IMPL_NAMESPACE::NumParseError::FloatUnderflow;
  } else {
    return other;
  }
}

static PJ_FORCE_INLINE PJ_IMPL_NAMESPACE::NumParseError getRangeErrorType(const pj::fnat_t value) {
  if (value == UNDER_MAX_B || value == UNDER_MIN_B) {
    return PJ_IMPL_NAMESPACE::NumParseError::FloatUnderflow;
  }
  return checkOverUnderFlowImpl(value, PJ_IMPL_NAMESPACE::NumParseError::Invalid);
}

static PJ_FORCE_INLINE PJ_IMPL_NAMESPACE::NumParseError checkOverUnderFlow(const pj::fnat_t value) {
  return checkOverUnderFlowImpl(value, PJ_IMPL_NAMESPACE::NumParseError::FloatNone);
}

/**
 * Errno must be set to 0 by the caller.
 */
static PJ_FORCE_INLINE PJ_IMPL_NAMESPACE::NumParseResult libcTryParseFloat(const char* const str) noexcept {
  static_assert(sizeof(ptrdiff_t) >= sizeof(uint16_t), "ptrdiff_t too small");

  // Reset errno.
  errno = 0;

  char* end;
  const auto value = strToFloatingValue(str, &end);

  const auto err = errno;


  PJ_IMPL_NAMESPACE::NumParseResult res;
  res.fValue = value;

  if (*end != '\0') {
    res.err = PJ_IMPL_NAMESPACE::NumParseError::Invalid;
    return res;
  }

  if (err == ERANGE) {
    res.err = getRangeErrorType(value);
    return res;
  }

  // Libc doesn't have to set ERANGE on underflow.
  res.err = checkOverUnderFlow(value);

  return res;
}

PJ_IMPL_NAMESPACE::NumParseResult PJ_IMPL_NAMESPACE::tryParseNumber(const char* str) noexcept {
  static_assert(sizeof(int32_t) <= sizeof(long), "strtol too small to fit a uint32_t");
  static_assert(INT32_MAX <= LONG_MAX, "strtol too small to fit a uint32_t");
  static_assert(INT32_MIN >= LONG_MIN, "strtol too small to fit a uint32_t");

  // Reset errno.
  errno = 0;

  char* end;
  const auto res_long = strtol(str, &end, 10);

  const auto err = errno;

  NumParseResult r;

  if (*end == '.' || (*end & 0x5F) == 'E') {
    // Looks like a floating point number. Re-try parsing it as such.
    return libcTryParseFloat(str);
  } else if (*end != '\0') {
    r.err = NumParseError::Invalid;
    return r;
  }

  if (err == ERANGE) {
    r.err = NumParseError::IntegerOverflow;
    r.iValue = res_long > 0 ? INT32_MAX : INT32_MIN;
    return r;
  }

  if (res_long > INT32_MAX) {
    r.err = NumParseError::IntegerOverflow;
    r.iValue = INT32_MAX;
  } else if (res_long < INT32_MIN) {
    r.err = NumParseError::IntegerOverflow;
    r.iValue = INT32_MIN;
  } else {
    r.err = NumParseError::IntegerNone;
    r.iValue = res_long;
  }

  return r;
}

#endif
